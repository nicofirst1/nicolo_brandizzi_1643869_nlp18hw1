import os
import pickle

import numpy as np
import tensorflow as tf
import tqdm
from six.moves import xrange
from termcolor import colored

from HomeWork1.Word2Vec.data_processing import read_data_parallel, read_analogies, save_vectors, \
    build_dataset, generate_batch_cbow, build_dataset_freq, read_data_shuffle
from HomeWork1.Word2Vec.evaluation import evaluation
from HomeWork1.Utils.data_analysis import plot_with_labels
from HomeWork1.Utils.utils import beep, save_good_model, read_dictionary


class Word2Vec:
    """
    Class responsable for the embedding generation
    """

    def __init__(self, configuration):
        """
        Save some variables and reset the graph
        :param configuration:
        """

        # reset tensorflow graph
        tf.reset_default_graph()
        # proto config
        self.proto_config=tf.ConfigProto(log_device_placement=True)
        self.proto_config.gpu_options.allow_growth = True

        self.config = configuration
        self.valid_examples = np.random.choice(self.config.valid_window, self.config.valid_size, replace=False)

        self.data, self.dictionary, self.reverse_dictionary = self.build_dataset()
        self.questions = read_analogies(self.config.ANALOGIES_FILE, self.dictionary)

        # save the graph
        self.graph = tf.Graph()

        # open the graph and build the variables
        with self.graph.as_default():
            # === BUILDING VARIABLES
            self.x_train, self.y_train, self.valid_dataset = self.build_inputs()
            self.embeddings, self.lookups = self.build_embeddings()
            self.loss = self.build_loss()
            self.optimizer = self.build_optimizer()
            self.normalized_embeddings, self.similarity = self.build_similarity()
            self.accuracy = self.build_accuracy()

            # === SUMMARY AND LOGS

            # Merge all summaries.
            self.merged = tf.summary.merge_all()

            # Add variable initializer.
            self.initializer = tf.global_variables_initializer()

            # create the writer
            self.file_writer = tf.summary.FileWriter(self.config.LOG_DIR, self.graph)

            # save the evaluator
            self.eval_ = evaluation(self.normalized_embeddings, self.dictionary, self.questions,
                                    self.reverse_dictionary)

    # === BUILDING GRAPH

    def build_inputs(self):
        # ===INPUTS
        with tf.name_scope('inputs'):
            # Select the right size for the training_inputs depending on the model chosen
            x_train = tf.placeholder(tf.int32,
                                     shape=[self.config.batch_size, self.config.window_size * 2],
                                     name="x_train")  # skipgram

            y_train = tf.placeholder(tf.int32, shape=[self.config.batch_size, 1], name="y_train")
            valid_dataset = tf.constant(self.valid_examples, dtype=tf.int32, name="valid_dataset")

        return x_train, y_train, valid_dataset

    def build_embeddings(self):

        # ===EMBEDDINGS
        with tf.name_scope('embeddings'):
            # build the embeddings
            embeddings = tf.Variable(
                tf.random_uniform([self.config.vocabulary_size, self.config.embedding_size], -1.0, 1.0),
                name="embeddings")
            lookups = tf.nn.embedding_lookup(embeddings, self.x_train, name="lookups")

        return embeddings, lookups

    def build_loss(self):
        with tf.name_scope('loss_variables'):
            # ===WEIGHTS
            # initialazing the weights with a truncated normal,
            # this prevent both the vanishing gradient descent in case of initializing with zeroes
            # and prevents an infinite range of values in case of using the simple normal distribution
            nce_weights = tf.Variable(
                tf.truncated_normal(
                    [self.config.vocabulary_size, self.config.embedding_size],
                    stddev=1.0 / np.sqrt(self.config.embedding_size)), name="nce_weights")

            # ===BIASES
            nce_biases = tf.Variable(tf.zeros([self.config.vocabulary_size]), name="nce_biases")
            # === REGULARIZATION
            l2 = tf.nn.l2_loss(nce_weights, name="l2")

        # ===LOSS
        with tf.name_scope('loss'):
            loss = tf.reduce_mean(
                tf.nn.nce_loss(weights=nce_weights, biases=nce_biases,
                               inputs=tf.reduce_mean(self.lookups, 1),
                               labels=self.y_train,
                               num_sampled=self.config.neg_sampled,
                               num_classes=self.config.vocabulary_size, name="loss"),
                name="reduce_mean") + l2 * self.config.beta  # cbow

            self.add2summary(loss, "loss")

        return loss

    def build_optimizer(self):
        # === OPTIMIZER
        # nce_loss
        with tf.name_scope('optimizer'):
            optimizer = tf.train.GradientDescentOptimizer(learning_rate=self.config.learning_rate,
                                                          name="optimizer").minimize(self.loss)

        return optimizer

    def build_similarity(self):
        with tf.name_scope("similarity"):
            # Compute the cosine similarity between minibatch examples and all embeddings.

            norm = tf.sqrt(tf.reduce_sum(tf.square(self.embeddings), 1, keepdims=True), name="norm")

            normalized_embeddings = self.embeddings / norm
            valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings,
                                                      self.valid_dataset)
            similarity = tf.matmul(
                valid_embeddings, normalized_embeddings, transpose_b=True, name="similarity")

        return normalized_embeddings, similarity

    def build_accuracy(self):
        # === ACCURANCY
        with tf.name_scope("accuracy"):
            accuracy = tf.placeholder(name="accuracy", dtype=tf.float64, shape=[])
            self.add2summary(accuracy, "Accurancy")

        return accuracy

    # === UTILS

    def read_data(self):
        """
        Read the train_data form the train direcotry
        :return: a list of words
        """

        print(colored("Reading train_data from files...", on_color="on_yellow", color="blue"))

        if self.config.parallel:
            words = read_data_parallel(self.config.TRAIN_DIR,
                                       self.config.CUSTOM_DATA,
                                       filter_=self.config.filter_,
                                       load=self.config.load)
        else:
            words = read_data_shuffle(self.config.TRAIN_DIR,
                              filter_=self.config.filter_,
                              domain_words=self.config.domain_words)

        print(colored("...train_data read", on_color="on_yellow", color="blue"))

        return words

    def build_dataset(self):
        """
        Build the dataset from the list of words
        :returns:
            train_data: the converted dataset where a word match an id
            dictionary: the dictionary of type word:id
            reverse_dictionary: the dictionary of type id:word
        """

        print(colored("Building train_data...", on_color="on_yellow", color="blue"))
        # read the train_data from the files
        words = self.read_data()

        # restore the dictionary
        if self.config.restore:
            # try to load the file
            try:

                data, dictionary, reverse_dictionary = read_dictionary(self.config.OPERATORS_DIR + "dictionary.pickle",
                                                                       words)

                return data, dictionary, reverse_dictionary
            # if the dictionary is not there tell the user and build it
            except FileNotFoundError:
                print(colored("Cannot find pickle dict, but self.config.restore is True...", color="red"))
                self.config.restore = False

        # build the train_data, dictionary and reverse_dictionary
        if self.config.by_freq:
            data, dictionary, reverse_dictionary = build_dataset_freq(words, self.config.vocabulary_size)
        else:
            data, dictionary, reverse_dictionary = build_dataset(words, self.config.vocabulary_size)

        # write labels to file
        with open(self.config.MODEL_DIR + 'metadata.tsv', 'w+') as f:
            for word in dictionary.keys():
                line_out = "%s\n" % word
                f.write(line_out)

        # save the dictionary with pickle
        with open(self.config.OPERATORS_DIR + "dictionary.pickle", "wb") as file:
            pickle.dump(dictionary, file, protocol=pickle.HIGHEST_PROTOCOL)

        print(colored("...train_data built", on_color="on_yellow", color="blue"))

        return data, dictionary, reverse_dictionary

    def restore_model(self, sess):
        self.saver = tf.train.import_meta_graph(self.config.MODEL_DIR + "model.ckpt")
        self.saver.restore(sess, self.config.LOG_DIR)

    def add2summary(self, tensor, name, type_='scalar'):
        """
        Add a parameter to the summary
        :param tensor:tf.tensor, the tensor to add
        :param name: str, the tensor name
        :param type: str, the type of summary to be add (Default is 'scala' but can be 'histogram' too)
        :return: None
        """

        if type_ == "scalar":
            tf.summary.scalar(name, tensor)

        elif type_ == "histogram":
            tf.summary.histogram(name, tensor)

        else:
            raise AttributeError(f"No type for {type_}")

    def save_results(self, tot_q, correct_q, loss):

        if tot_q == 0:
            tot_q = -1

        with open(self.config.INFOS_DIR + "results.txt", "w+") as file:
            file.write(f"Accuracy % : {correct_q/tot_q*100}\n")
            file.write(f"Accuracy all : {correct_q}/{tot_q}\n")
            file.write(f"Loss : {loss}\n")

    def print_k_near(self):
        """Print the k near words to a given one
        """

        sim = self.similarity.eval()
        for i in xrange(self.config.valid_size):
            valid_word = self.reverse_dictionary[self.valid_examples[i]]
            top_k = 8  # number of nearest neighbors
            nearest = (-sim[i, :]).argsort()[1:top_k + 1]
            log_str = 'Nearest to %s:' % valid_word
            for k in xrange(top_k):
                close_word = self.reverse_dictionary[nearest[k]]
                log_str = '\r%s %s,' % (log_str, close_word)
            print(colored(log_str, color="green"))

    # === TRAIN

    def train(self):
        """
        Train the graph
        :return:
        """

        with tf.Session(graph=self.graph,config= self.proto_config) as session:

            # decide between initializing a new session or restoring the previous one
            if self.config.restore:
                # Create a saver.
                self.saver = tf.train.import_meta_graph(self.config.MODEL_DIR + "model.ckpt.meta")
                self.saver.restore(session, self.config.MODEL_DIR + "model.ckpt")

                # resotre the operators
                self.embeddings = self.graph.get_tensor_by_name("embeddings/embeddings:0")
                self.optimizer = self.graph.get_tensor_by_name("optimizer/optimizer:0")

            else:
                # Initialize all tf varaibles
                self.initializer.run()
                # create the saver object
                self.saver = tf.train.Saver()

            print(colored('Training Data', on_color="on_yellow", color="blue"))

            # initialize local varaibles
            average_loss = 0
            bar = tqdm.tqdm(range(self.config.num_steps))
            data_index = 0
            save_step = round(self.config.num_steps * self.config.save_step)  # the step in which to save the varaibles
            accuracy = 0.0

            # === TRAINING STARTS
            for step in bar:
                try:
                    # Choose the model
                    x_batch, y_batch, data_index = generate_batch_cbow(
                        self.config.batch_size, self.config.window_size, self.data, data_index)

                    # feed the train_data to the graph
                    feed_dict = {self.x_train: x_batch, self.y_train: y_batch, self.accuracy: accuracy}

                    # Define metadata variable.
                    run_metadata = tf.RunMetadata()

                    # Perfrom the step and get bacj the loss value and the summary of the graph
                    _, summary, loss_val = session.run(
                        [self.optimizer, self.merged, self.loss],
                        feed_dict=feed_dict,
                        run_metadata=run_metadata)
                    average_loss += loss_val

                    # Write the summary
                    self.file_writer.add_summary(summary, step)

                    # Write metadata
                    if step == (self.config.num_steps - 1):
                        self.file_writer.add_run_metadata(run_metadata, 'step%d' % step)

                    # Every now and then print some infos
                    if step % (save_step // 10) == 0:
                        if step > 0:
                            # estimate the average loss
                            average_loss /= step

                        # get the accurnacy and save it to the tf variable
                        t, c = self.eval_.eval(session)
                        if t > 0: accuracy = c / t * 100
                        # print loss
                        print(colored(f"\rAvagarege loss {average_loss}", color="yellow"))
                        average_loss = 0

                        # save the model
                        self.saver.save(session, self.config.MODEL_DIR + "model.ckpt")

                    # Print the N closer words to a given one
                    if step % save_step == 0:
                        self.print_k_near()
                except KeyboardInterrupt:
                    break

                # === TRAINING ENDS

            # get the normalized embeddings and save them

            normalized_embd = self.normalized_embeddings.eval()
            embed = self.embeddings.eval()

            save_vectors(self.config.OPERATORS_DIR, normalized_embd, "normalized_embed")
            save_vectors(self.config.OPERATORS_DIR, embed, "embed")

            # Save the model for checkpoints.
            self.saver.save(session, os.path.join(self.config.MODEL_DIR, 'model.ckpt'))

            total, correct = self.eval_.eval(session)
            self.eval_.build_eval_graph()

        # Write corresponding labels for the embeddings.
        with open(self.config.LOG_DIR + 'metadata.tsv', 'w') as f:
            for i in xrange(self.config.vocabulary_size):
                try:
                    f.write(self.reverse_dictionary[i] + '\n')
                except KeyError:
                    pass

        self.file_writer.close()
        self.save_results(total, correct, average_loss)
        beep()

        plot_with_labels(self.config.INFOS_DIR, normalized_embd, self.reverse_dictionary, "normalized_embed")
        plot_with_labels(self.config.INFOS_DIR, embed, self.reverse_dictionary, "embed")

        save_good_model(self.config.SUPER_MODEL_DIR)
