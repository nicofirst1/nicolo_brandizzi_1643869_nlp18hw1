import os
import ujson




class W2vConfig:
    """
    Configuration class to store all the configuration variables

    PATH VARIABLES

        DATA DIRS
            DATA_DIR: the direcotry in which the train_data is stored
            TRAIN_DIR: the dir in which the train train_data is stored
            VALID_DIR: the dir in which the test train_data is stored
            ANALOGIES_FILE: the file contaning the analogies
            CUSTOM_DATA: the dir in which the custom train_data is stored

        MODEL DIRS
            SUPER_MODEL_DIR : the dir in which all the model train_data is stored
            LOG_DIR: the dir in which to save the logs from tensorflow
            MODEL_DIR : the dir in which the model chekpoints and metadata are saved
            OPERATORS_DIR : the dir in which the tensors are stored
            INFOS_DIR : the dir in which some infos are stored (such as results, attributes...)


    READING DATA VARIABLES
        load: bool, true if you wish to load some custom train_data
        filter: bool, true if you wish to filter out the train_data (check the data_preprocessing/filter_document function)
        parallel: bool, true if you want to read from a domain using multiple threads
        domain_words: int, the maximum number of words to read from each domain (-1 to read them all)

    DATASET BUILDING VARIABLES
        vocabulary_size: int, the number of words you want to use for your vocabulary
        by_freq: bool, this parameter will change the way you build the dictionary
            if set to false: then the N mosto common words will be chosen (with N=vocabulary_size)
            else a frequent approach will be used (check out data_preprocessing/build_dataset fucnction)


    BATCH VARIABLES
        batch_size: int, the size of the batch
        window_size: int, how many words to consider left and right
        input_reuse: int, how many times to reuse an input to generate a label
        num_sampled: int, number of negative examples to sample

    MODEL ACCURANCY VARIABLES
        valid_size: int, random set of words to evaluate similarity on
        valid_window = int, only pick dev samples in the head of the distribution


    TRAINING VARAIBLES
        embedding_size:  int,  dimension of the embedding vector
        num_step: int, number of step for the training phase
        learning_rate: float, the learning rate for the optimizer
        beta, float, regularization parameter to be add to the loss function

    OTHER
        save_step: determine how many step to save the model, it will be (curr_step%num_steps*save_step)==0
        model : the kid of model to use (cbow, skipgram)
        restore: bool, restore the model from the SUPER_MODEL_DIR

    """

    def __init__(self):
        # Data directory
        self.DATA_DIR = "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/"
        self.TRAIN_DIR = self.DATA_DIR + "datasets/DATA/TRAIN"
        self.VALID_DIR = self.DATA_DIR + "datasets/DATA/DEV"
        self.CUSTOM_DATA = self.DATA_DIR + "Custom_Data/"
        self.ANALOGIES_FILE = self.DATA_DIR + "datasets/eval/questions-words.txt"


        #model direcotry

        self.SUPER_MODEL_DIR="Word2Vec/Model/"
        self.LOG_DIR =self.SUPER_MODEL_DIR+ "Logs/"
        self.MODEL_DIR =self.SUPER_MODEL_DIR+ "Model/"
        self.OPERATORS_DIR= self.SUPER_MODEL_DIR+"Operators/"
        self.INFOS_DIR=self.SUPER_MODEL_DIR+"Infos/"

        #create the model dirs if not present
        self.create_dirs()


        # reading dataset configs
        self.load = False
        self.filter_ = True
        self.parallel = False
        self.domain_words = 3000000
        self.filter_dict={'remove_stopwords':True, 'remove_punctuation':True,
                          'remove_numbers':True, 'min_char':2, 'deep_clean':True,
                          'stemming':False, 'lemmatize':False,'remove_non_english':False,}



        # Building dataset
        self.vocabulary_size = 70000
        self.by_freq = False

        # batch varaiables
        self.batch_size = 256
        self.window_size = 4
        self.input_reuse = 2
        self.neg_sampled = 12

        # Validation variables
        self.valid_size = 16
        self.valid_window = 100

        # training
        self.embedding_size = 170
        self.num_steps = 8000000#best is 8M, acurancy should be>=1.3 for 300k
        self.learning_rate = 1.0
        self.beta = 0.0

        # other
        self.save_step = 0.1
        self.restore = False
        self.loss="nce"


    def save_attributes(self):
        """
        Saves all the attributes in the model direcotry
        :return: None
        """

        if not self.restore:
            with open(self.INFOS_DIR + "attributes.json", "w+") as file:
                ujson.dump(self.__dict__,file)

    def create_dirs(self):
        os.makedirs(self.SUPER_MODEL_DIR,exist_ok=True)
        os.makedirs(self.LOG_DIR,exist_ok=True)
        os.makedirs(self.MODEL_DIR,exist_ok=True)
        os.makedirs(self.OPERATORS_DIR,exist_ok=True)
        os.makedirs(self.INFOS_DIR,exist_ok=True)

