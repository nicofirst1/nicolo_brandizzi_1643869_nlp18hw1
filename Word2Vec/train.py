from HomeWork1.Word2Vec.Configurations import *
from HomeWork1.Word2Vec.Word2Vec import *

from HomeWork1.Utils.utils import clean_logs




w2v_config=W2vConfig()

clean_logs(w2v_config.SUPER_MODEL_DIR)

w2v_config.save_attributes()




word2vec=Word2Vec(w2v_config)
word2vec.train()

