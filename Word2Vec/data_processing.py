import os
import pickle
import random
import string
from multiprocessing import Pool

import collections
import nltk
import numpy as np
from nltk import PorterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from termcolor import colored

from HomeWork1.Utils.utils import tfidf

CHACHED_STOPWORDS = stopwords.words('english')

ENGLISH_WORDS = set(nltk.corpus.words.words())


# =============BATCHES CREATOR===================


def generate_batch_cbow(batch_size, skip_window, data, data_index):
    """
    Generate a batch form the data
    :param batch_size: int, the size of the batch
    :param skip_window: int, the sie of the skip window (i.e. how many words to look for around a specific one)
    :param data: vector, the data to generate a batch from
    :param data_index: the index of the data for all the batcges
    :returns:
        batch: the batched data
        labels: the label corresponding to the bathced data
        data_index: the general index of the data
    """
    # get the span size
    span = 2 * skip_window + 1
    # initialize the arrays
    batch_array = np.ndarray(shape=(batch_size, span - 1), dtype=np.int32)
    labels_array = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
    buffer = collections.deque(maxlen=span)
    # append new entries to the buffer and update the data_iindex
    for _ in range(span):
        buffer.append(data[data_index])
        data_index = (data_index + 1) % len(data)

    # generate the batch and label arrays
    for i in range(batch_size):
        buffer_list = list(buffer)
        labels_array[i, 0] = buffer_list.pop(skip_window)
        batch_array[i] = buffer_list
        buffer.append(data[data_index])
        data_index = (data_index + 1) % len(data)
    return batch_array, labels_array, data_index


# =============UTILS===================


def save_vectors(dir, embeddings, name):
    """
    Save embedding vectors in a suitable representation for the domain identification task
    :param dir: the directory in which to save the embeddings
    :param embeddings: the embeddings to save
    :param name: the name of the embedding's file
    :return: None
    """

    with open(dir + f"{name}.pickle", "wb") as file:
        pickle.dump(embeddings, file, pickle.HIGHEST_PROTOCOL)


def read_analogies(file, dictionary):
    """
    Reads through the analogy question file.

    :param file:
    :param dictionary:
    :returns:
        questions: a [n, 4] numpy array containing the analogy question's word ids.
        questions_skipped: questions skipped due to unknown words.
    """
    questions = []
    questions_skipped = 0
    with open(file, "r") as analogy_f:
        for line in analogy_f:
            if line.startswith(":"):  # Skip comments.
                continue
            words = line.strip().lower().split(" ")
            ids = [dictionary.get(str(w.strip())) for w in words]
            if None in ids or len(ids) != 4:
                questions_skipped += 1
            else:
                questions.append(np.array(ids))
    print("Eval analogy file: ", file)
    print("Questions: ", len(questions))
    print("Skipped: ", questions_skipped)
    return np.array(questions, dtype=np.int32)


def filter_document(words, remove_stopwords=True, remove_punctuation=True, remove_numbers=True, min_char=2,
                    deep_clean=True,
                    stemming=False, lemmatize=False, remove_non_english=False):
    """
    Filter out the words in a document based on various parameters
    :param min_char: int, minimum number of char for a word to be taken into account
    :param remove_numbers: bool, remove any number
    :param words: the words that compose a line
    :param remove_stopwords: bool for stopwords removing (default true)
    :param remove_punctuation:  bool for punctuation removing (default true)
    :param stemming:  bool for stemming (default False)
    :param lemmatize:  bool for lemmatizing (default False)
    :param deep_clean: bool, for removing avery word which do not contain ONLY letters or ONLY numbers
    :param remove_non_english: bool, dor removing any word which is not an english word

    :return: the line represented as a list of words
    """

    if stemming and lemmatize:
        raise ValueError("You cannot have both stemming and lemmatize as true")

    stemmer = PorterStemmer()
    lemma = WordNetLemmatizer()

    translator = str.maketrans('', '', string.punctuation)

    # remove stopwords
    if remove_stopwords:
        words = [word for word in words if word not in (CHACHED_STOPWORDS)]

    if not words: return []

    # remove words which contains '_' and punctuation
    if remove_punctuation:
        # words = [re.sub(r'[^\w\s]', '', word) for word in words if "_" not in word]
        words = [word.translate(translator) for word in words if "_" not in word]

    if not words: return []

    # remove every word that contains a number
    if remove_numbers:
        words = [word for word in words if not word.isdigit()]

    if not words: return []

    # remove every word which contains both numbers and letters
    if deep_clean:
        # words = [word for word in words if not re.match(r"(?!^\d+$)(?!^[A-z]+$)^.+$", word)]
        words = [word for word in words if word.isalpha() or word.isdigit()]

    if not words: return []

    # remove non english words
    if remove_non_english:
        words = [word for word in words if word in ENGLISH_WORDS]

    if not words: return []

    # remove any word which have a len minor than min_char
    if min_char > 0:
        words = [word for word in words if len(word) > min_char]

    if not words: return []

    # stem words
    if stemming:
        words = [stemmer.stem(word) for word in words]

    # lemmatize
    elif lemmatize:
        words = [lemma.lemmatize(word) for word in words]

    return words


# =============DATASET BUILDING===================

def build_dataset(words, vocab_size):
    assert len(words) > vocab_size

    count = [['UNK', -1]]
    c = collections.Counter(words)
    print(colored(f"Found {len(c)} different words in the corpora, {round(len(c)/vocab_size)} times the vocab_size",
                  color="yellow"))
    count.extend(c.most_common(vocab_size - 1))
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()
    unk_count = 0
    for word in words:
        index = dictionary.get(word, 0)
        if index == 0:  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    print(f"UNK {round(count[0][1]/sum(elem[1] for elem in count)*100,2)}%")
    return data, dictionary, reverse_dictionary


def build_dataset_freq(words, vocab_size):
    """This function is responsible of generating the dataset and dictionaries.
        While constructing the dictionary take into account the unseen words by
        retaining the rare (less frequent) words of the dataset from the dictionary
        and assigning to them a special token in the dictionary: UNK. This
        will train the model to handle the unseen words.

        Parameters
            words: a list of words
            vocab_size:  the size of vocabulary

        Return values
            train_data: list of codes (integers from 0 to vocabulary_size-1).
            This is the original text but words are replaced by their codes
            dictionary: map of words(strings) to their codes(integers)
            reverse_dictionary: maps codes(integers) to words(strings)

    """

    # determine the size of the vocabulary
    # create a counter obj to count the frequencies of the words
    c = collections.Counter(words)

    print(colored(f"Vocabulary initial size : {len(c)}", color="cyan"))
    # drop some words if they are more than the vocab size
    if len(c) > vocab_size:
        c = drop_by_freq_easy(c, vocab_size - 1)
    else:
        c = [(w, v) for w, v in c.items()]

    count = [['UNK', -1]]
    count.extend(c)
    print(colored(f"Vocabulary size after filtering : {len(count)}", color="cyan"))

    del c

    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()
    unk_count = 0
    for word in words:
        index = dictionary.get(word, 0)
        if index == 0:  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    print(colored(f"UNK {round(count[0][1]/sum(elem[1] for elem in count)*100,2)}%", color="cyan"))
    return data, dictionary, reverse_dictionary


def build_dataset_freq_shuffled(documents, vocab_size):
    """This function is responsible of generating the dataset and dictionaries.
        While constructing the dictionary take into account the unseen words by
        retaining the rare (less frequent) words of the dataset from the dictionary
        and assigning to them a special token in the dictionary: UNK. This
        will train the model to handle the unseen words.

        Parameters
            words: a list of words
            vocab_size:  the size of vocabulary

        Return values
            train_data: list of codes (integers from 0 to vocabulary_size-1).
            This is the original text but words are replaced by their codes
            dictionary: map of words(strings) to their codes(integers)
            reverse_dictionary: maps codes(integers) to words(strings)

    """

    # determine the size of the vocabulary
    # create a counter obj to count the frequencies of the words
    c = collections.Counter([word for sublist in documents for word in sublist])

    print(colored(f"Vocabulary initial size : {len(c)}", color="cyan"))
    # drop some words if they are more than the vocab size
    if len(c) > vocab_size:
        c = drop_by_freq_tfidf(documents, vocab_size - 1)
    else:
        c = [(w, v) for w, v in c.items()]

    count = [['UNK', -1]]
    count.extend(c)
    print(colored(f"Vocabulary size after filtering : {len(count)}", color="cyan"))

    del c

    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()
    unk_count = 0
    for word in word:
        index = dictionary.get(word, 0)
        if index == 0:  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    print(colored(f"UNK {round(count[0][1]/sum(elem[1] for elem in count)*100,2)}%", color="cyan"))
    return data, dictionary, reverse_dictionary


def drop_by_freq_easy(words_counter, vocab_size, std_perc=1):
    """
    Calculates the frequencies for each word, and uses the mean value shifted by std*std_perc to get the words which
     are closer to the mean threshold
    :param words_counter: a list of words
    :param vocab_size: the maximum vocabulary size
    :param std_perc: float, the % of std to sum the threshold by
    :return: a list of words
    """
    # get the total number of words
    words_num = len(words_counter)

    one_repetition = len([w for w in words_counter.items() if w[1] == 1])

    # remove the words which appeare just one time
    if words_num - one_repetition >= vocab_size:
        words_counter = {w: v for w, v in words_counter.items() if v != 1}

    else:
        to_pop = words_num - vocab_size
        for w, v in words_counter.items():
            if v == 1 and to_pop:
                del words_counter[w]
                to_pop -= 1

    # get the frequencies of each word
    word_frequencies = {word: count / words_num for word, count in words_counter.items()}

    # get all the frequencies
    freqs = [value for _, value in word_frequencies.items()]

    # freqs = freqs / np.linalg.norm(freqs)

    # get the mean threshold
    threshold = np.mean(freqs)
    std = np.std(freqs)

    print(f"mean : {threshold}, std: {std}")

    # move the threashold closer the the lest (most common words)
    threshold += std * std_perc

    # calculating distance between mean and frequency
    dist = {word: pow(d - threshold, 2) for word, d in word_frequencies.items()}

    # getting the elements which are the closest to the mean value
    dist = sorted([(w, v) for w, v in dist.items()], key=lambda x: x[1], reverse=True)

    # return the N words
    return dist[:vocab_size]


def drop_by_freq_tfidf(documents, vocab_size, std_perc=1):
    """
    Calculates the frequencies for each word, and uses the mean value shifted by std*std_perc to get the words which
     are closer to the mean threshold
    :param words_counter: a list of words
    :param vocab_size: the maximum vocabulary size
    :param std_perc: float, the % of std to sum the threshold by
    :return: a list of words
    """

    words = [word for sublist in documents for word in sublist]
    words_counter = collections.Counter(words)

    # get the total number of words
    words_num = len(words_counter)

    one_repetition = len([w for w in words_counter.items() if w[1] == 1])

    # remove the words which appeare just one time
    if words_num - one_repetition >= vocab_size:
        words_counter = {w: v for w, v in words_counter.items() if v != 1}

    else:
        to_pop = words_num - vocab_size
        for w, v in words_counter.items():
            if v == 1 and to_pop:
                del words_counter[w]
                to_pop -= 1
        return words_counter

    # get the frequencies of each word
    word_tfidf = {word: tfidf() for word, count in words_counter.items()}

    # get all the frequencies
    freqs = [value for _, value in words.items()]

    # freqs = freqs / np.linalg.norm(freqs)

    # get the mean threshold
    threshold = np.mean(freqs)
    std = np.std(freqs)

    print(f"mean : {threshold}, std: {std}")

    # move the threashold closer the the lest (most common words)
    threshold += std * std_perc

    # calculating distance between mean and frequency
    dist = {word: pow(d - threshold, 2) for word, d in words.items()}

    # getting the elements which are the closest to the mean value
    dist = sorted([(w, v) for w, v in dist.items()], key=lambda x: x[1], reverse=True)

    # return the N words
    return dist[:vocab_size]


# =============FILE READING===================

def parallel_reading(f):
    """
    Perform words cleaning on a single file
    :param f: the file on which the cleaning is to be performed
    :return: a list of words from the file
    """

    parall_data = []

    # read the train_data form all documents in domain
    if f.endswith(".txt"):
        # read the train_data form a specific document
        with open(f) as file:
            # for every line in the file
            for line in file.readlines():
                # split the line by words
                split = line.lower().strip().split()
                parall_data += filter_document(split)

    return parall_data


def read_data_parallel(directory, custom_data, load=True, filter_=False):
    """
    Read the train_data
    :param directory:  the direcotry in which the train_data is stored
    :return: a list of words from all the train_data
    """

    def get_data_from_corpora(custom_data, filter_, save=False):
        """
        Inner function to read and save train_data from corpora

        :param custom_data: the direcotry in which the custom train_data is stored
        :param filter: bool, filter the corpora if true
        :param save: bool (Default False), save the train_data into a file
        :return: a list of words from all the domains
        """

        # remove the previous custom train_data
        if save:
            with open(os.path.join(custom_data, data_file), "w+") as file:
                file.write("")

        data = []
        to_read = len(os.listdir(directory))
        idx = 0

        # create a pool
        pool = Pool(10)

        # for every domain in the dataset
        for domain in os.listdir(directory):
            # get the domain path
            path = os.listdir(os.path.join(directory, domain))
            path = [os.path.join(f"{directory}/{domain}", elem) for elem in path]
            data += pool.map(parallel_reading, path)
            idx += 1
            print(f"\rReading domain {domain}: {idx}/{to_read}", end="")

        if save:
            print(f"Saving train_data in {custom_data}")
            to_write = len(data)
            idx = 0
            for words in data:
                with open(os.path.join(custom_data, data_file), "a+") as file:
                    file.write("\n")
                    file.write("\n".join(words))
                idx += 1

                if idx % 500 == 0:
                    print(f"\rSaved file {idx}/{to_write}", end="")

        data = [item for sublist in data for item in sublist]
        print("")

        return data

    data_file = "filtered_data.txt"

    # try to load the train_data from the custom_data dir
    if load:
        try:
            # read the lines of the file
            with open(os.path.join(custom_data, data_file), "r+") as file:
                to_read = len(file.readlines())
            # if there are no lines the file is empty, so take the words from the original dir
            if to_read == 0:
                raise FileNotFoundError

            # read the file
            with open(os.path.join(custom_data, data_file), "r+") as file:
                print("Loading train_data from custom file...")
                data = []
                idx = 0
                # read a line, strip the newline and pass if empty
                for line in file:
                    line = line.strip("\n")
                    if line:
                        data.append(line)

                    idx += 1
                    if idx % 10000 == 0:
                        print(f"\rRead {round(idx/to_read*100,2)}% of the file", end="")

                print("\n...train_data loaded")
        except FileNotFoundError:
            data = get_data_from_corpora(custom_data, filter_, save=True)

    # generate the train_data from the data_dir
    else:
        data = get_data_from_corpora(custom_data, filter_)

    print("")
    return data


def read_data_shuffle(directory, domain_words, filter_=True):
    """
       Read the train_data form the direcotry and split it by domain
       :param directory:  the direcotry in which the train_data is stored
       :return: a dictionary in which the key is the domain name and the value:
            a list of words
                in a list of sentences
                    in a list of documents
       """

    data = {}  # a dictionary with
    to_read = len(os.listdir(directory))
    idx = 0
    # read the train_data from all domains

    for domain in os.listdir(directory):
        # determine a limit for each domain
        limit = domain_words
        data[domain] = []
        data_domain = []

        # shuffle the files in each domain
        files = os.listdir(os.path.join(directory, domain))
        random.shuffle(files)
        for f in files:
            data_document = []
            # if the limit for the domain has been reached pass to the next one
            if limit == 0: break

            if f.endswith(".txt"):
                # read the train_data form a specific document
                with open(os.path.join(directory, domain, f)) as file:

                    # for every line in the file
                    for line in file.readlines():

                        # split the line by words
                        split = line.lower().strip().split()

                        if filter_:
                            split = filter_document(split)

                        # if the limit has been reached
                        if limit > 0 and limit - len(split) < 0:
                            # take just the words which does not exceed the limit
                            split = split[:limit]
                            # add them to the train_data
                            data_document += split
                            # set the limit to zero to break
                            limit = 0
                        # else update decrease the limit
                        elif limit > 0:
                            limit -= len(split)

                        # if the limit hasn't been reached yet
                        if limit > 0 or limit == -1:
                            data_document += split
                        # if the limit has been reached exit
                        if limit == 0:
                            break

                    # clean train_data document
                    data_document = [elem for elem in data_document if elem]

            data_domain.append(data_document)
            # clean train_data domain
            data_domain = [elem for elem in data_domain if elem]
        data[domain] += data_domain

        idx += 1
        print(colored(f"\rReading file {idx}/{to_read}", color="cyan"), end="")

    print("")

    # flatten the list to get a list of documents
    data = [v for sublist in data.values() for v in sublist]
    # shuffle the document order
    random.shuffle(data)
    # flatten the documents to get a list of wrods
    data = [v for sublist in data for v in sublist]

    return data


def read_data(directory, domain_words=-1, filter_=False):
    """
    Read the train_data
    :param directory:  the direcotry in which the train_data is stored
    :return: a list of words from all the train_data
    """

    data = []
    to_read = len(os.listdir(directory))
    idx = 0
    # read the train_data from all domains

    for domain in os.listdir(directory):
        # determine a limit for each domain
        limit = domain_words

        # shuffle the files in each domain
        files = os.listdir(os.path.join(directory, domain))
        random.shuffle(files)
        for f in files:
            # if the limit for the domain has been reached pass to the next one
            if limit == 0: break

            if f.endswith(".txt"):
                # read the train_data form a specific document
                with open(os.path.join(directory, domain, f)) as file:
                    # for every line in the file
                    for line in file.readlines():
                        # split the line by words
                        split = line.lower().strip().split()

                        if filter_:
                            split = filter_document(split)

                        # if the limit has been reached
                        if limit > 0 and limit - len(split) < 0:
                            # take just the words which does not exceed the limit
                            split = split[:limit]
                            # add them to the train_data
                            data += split
                            # set the limit to zero to break
                            limit = 0
                        # else update decrease the limit
                        elif limit > 0:
                            limit -= len(split)

                        # if the limit hasn't been reached yet
                        if limit > 0 or limit == -1:
                            data += split
                        # if the limit has been reached exit
                        if limit == 0:
                            break

        idx += 1
        print(colored(f"\rReading file {idx}/{to_read}", color="cyan"), end="")

    print("")
    return data


def read_data_original(directory, domain_words=-1):
    """
    Read the train_data into a list of strings.
    the domain_words parameters limits the number of words to be loaded per domain
    :param directory:
    :param domain_words:
    :return:
    """
    data = []
    for domain in os.listdir(directory):
        # for dirpath, dnames, fnames in os.walk(directory):
        limit = domain_words
        for f in os.listdir(os.path.join(directory, domain)):
            if f.endswith(".txt"):
                with open(os.path.join(directory, domain, f)) as file:
                    for line in file.readlines():
                        split = line.lower().strip().split()
                        if limit > 0 and limit - len(split) < 0:
                            split = split[:limit]
                        elif limit != -1:
                            limit -= len(split)
                        if limit >= 0 or limit == -1:
                            data += split
    return data
