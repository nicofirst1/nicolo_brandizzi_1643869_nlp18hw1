import tensorflow as tf
from termcolor import colored


class evaluation():
    def __init__(self, nemb, dictionary, questions,reversed_dict):
        self.dictionary = dictionary
        self.questions = questions
        self.reversed_dit=reversed_dict
        self.nemb = nemb
        self.vocab_size = len(dictionary.keys())
        self.build_eval_graph()

    def build_eval_graph(self):
        """Build the eval graph."""

        with tf.name_scope("eval"):
            # Eval graph

            # Each analogy task is to predict the 4th word (d) given three
            # words: a, b, c.  E.g., a=italy, b=rome, c=france, we should
            # predict d=paris.

            # The eval feeds three vectors of word ids for a, b, c, each of
            # which is of size N, where N is the number of analogies we want to
            # evaluate in one batch.
            analogy_a = tf.placeholder(dtype=tf.int32)  # [N]
            analogy_b = tf.placeholder(dtype=tf.int32)  # [N]
            analogy_c = tf.placeholder(dtype=tf.int32)  # [N]

            # Normalized word embeddings of shape [vocab_size, emb_dim].
            #nemb = tf.nn.l2_normalize(self.emb_layer, 1)
            # Each row of a_emb, b_emb, c_emb is a word's embedding vector.
            # They all have the shape [N, emb_dim]
            a_emb = tf.nn.embedding_lookup(self.nemb, analogy_a)  # a's embs
            b_emb = tf.nn.embedding_lookup(self.nemb, analogy_b)  # b's embs
            c_emb = tf.nn.embedding_lookup(self.nemb, analogy_c)  # c's embs

            # We expect that d's embedding vectors on the unit hyper-sphere is
            # near: c_emb + (b_emb - a_emb), which has the shape [N, emb_dim].
            target = c_emb + (b_emb - a_emb)

            # Compute cosine distance between each pair of target and vocab.
            # dist has shape [N, vocab_size].
            dist = tf.matmul(target, self.nemb, transpose_b=True)

            # For each question (row in dist), find the top 10 words.
            _, pred_idx = tf.nn.top_k(dist, 10)

            # Nodes for computing neighbors for a given word according to
            # their cosine distance.
            nearby_word = tf.placeholder(dtype=tf.int32)  # word id
            nearby_emb = tf.gather(self.nemb, nearby_word)
            nearby_dist = tf.matmul(nearby_emb, self.nemb, transpose_b=True)
            nearby_val, nearby_idx = tf.nn.top_k(nearby_dist, min(1000, self.vocab_size))

            # Nodes in the construct graph which are used by training and
            # evaluation to run/feed/fetch.
            self._analogy_a = analogy_a
            self._analogy_b = analogy_b
            self._analogy_c = analogy_c
            self._analogy_pred_idx = pred_idx
            self._nearby_word = nearby_word
            self._nearby_val = nearby_val
            self._nearby_idx = nearby_idx

    def _predict(self, session, analogy):
        """Predict the top 4 answers for analogy questions."""
        idx, = session.run([self._analogy_pred_idx], {
            self._analogy_a: analogy[:, 0],
            self._analogy_b: analogy[:, 1],
            self._analogy_c: analogy[:, 2]
        })
        return idx

    def eval(self, session,save=False):
        """Evaluate analogy questions and reports accuracy."""

        # How many questions we get right at precision@1.
        correct = 0

        try:
            total = self.questions.shape[0]
        except AttributeError as e:
            raise AttributeError("Need to read analogy questions.")

        start = 0

        right = []
        wrong = []
        while start < total:
            limit = start + 2500
            sub = self.questions[start:limit, :]
            idx = self._predict(session, sub)
            start = limit

            for question in range(sub.shape[0]):
                right_prov=[]
                wrong_prov=[]

                q= sub[question, 3]

                #print("-".join([self.reversed_dit[elem] for elem in b]))
                #print("-".join([self.reversed_dit[elem] for elem in c]))
                for j in range(4):
                    pred= idx[question, j]
                    if pred ==q:
                        # Bingo! We predicted correctly. E.g., [italy, rome, france, paris].
                        right_prov.append(q)
                        correct += 1
                        break
                    elif pred in sub[question, :3]:
                        # We need to skip words already in the question.
                        continue
                    else:
                        # The correct label is not the precision@1
                        wrong_prov.append((pred , q))
                        #print("wrong\n")
                        break
                right.append(right_prov)
                wrong.append(wrong_prov)

        if total>0:print(colored(f"\nEval {correct}/{total}, accuracy = { correct * 100.0 / total}",color="yellow"))


        if save:
            self.save_answers(right,wrong)

        return total,correct


    def save_answers(self,correct,wrong):

        new_correct=[]
        for sublist in correct:
            prov=[self.reversed_dit[elem] for elem in sublist]
            new_correct.append(prov)

        new_wrong=[]
        for y_l, x_l in wrong:
            try:
                y_prov=[self.reversed_dit[elem] for elem in y_l]
                x_prov=[self.reversed_dit[elem] for elem in x_l]
                new_wrong.append((y_prov,x_prov))
            except KeyError: pass

        del wrong,correct

        file_name="right_wrong.txt"
        with open(file_name,"w+") as file:
            file.write("===================\n"
                       "===================\n"
                       "CORRECT ANSWERS\n"
                       "===================\n"
                       "===================\n")

            for sublist in new_correct:
                file.write("-".join(sublist)+"\n")

            file.write("===================\n"
                       "===================\n"
                       "WRONG ANSWERS\n"
                       "===================\n"
                       "===================\n")


            for y,x in new_wrong:
                file.write("-".join(y)+"\n")
                file.write("-".join(x)+"\n\n")

