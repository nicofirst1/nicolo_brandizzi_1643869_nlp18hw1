# Frame the Problem

## Objective
- [X] Implement a Word2Vec model in tensorflow
- [X] Train the model
- [X] Use the generated vectors to solve the domain-identification task 

## Current solutions
- Skip-gram : works well with small amount of the training data, represents well even rare words or phrases. 
- CBOW : several times faster to train than the skip-gram, slightly better accuracy for the frequent words

## Type of learning
- Supervised 
- Classification (34 categories)
- Offline

## Performance Measure

### Neural Net

- Analogical Reasoning task 
Depends mostly on the quality of the input data

### Classification

- Precision
- Recall
- ROC
- F-score?

# Explore the data

- 34 Categories
- For every categories we have multiple corpora 

# Prepare the data

## Punctuation removal

By removing the punctuation the total ammount of characters went down by 4%

## Stop words removal

By removing the stopword1s from nltk.corpus.stopwords.words('english') the total amount of characters went down by circa 25% while the 
total amount of words decreased by circa 40%.

## Save custom data
This step can save a lot of time since you do not need to preprocess the data everytime

# Build the dictionary

- [ ] Build a dictionary to map each word with an iteger
- [ ] Remove words that have a frequence less then a threshold


# Useful links
- [tensorflow implementation](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/word2vec/word2vec_basic.py)
- [skipgram implementation](https://towardsdatascience.com/learn-word2vec-by-implementing-it-in-tensorflow-45641adaf2ac)
- [multiple option](https://github.com/wangz10/tensorflow-playground/blob/master/word2vec.py#L105)
- [NCE LOSS](http://mccormickml.com/2016/04/19/word2vec-tutorial-the-skip-gram-model/)
- [regularization](https://arxiv.org/pdf/1508.03721.pdf)
- [using embeddings](https://www.slideshare.net/sujitpal/presentation-slides-77511261)
- [LSTM](https://towardsdatascience.com/lstm-by-example-using-tensorflow-feb0c1968537)
- [LSTM tensorflow](https://explosion.ai/blog/deep-learning-formula-nlp)
- [HAN](https://github.com/Carl-Xie/HAN-Tensorflow)
- [PPA](https://arxiv.org/pdf/1708.03629.pdf)
- [Tuning](https://arxiv.org/pdf/1707.08783.pdf)
- [sentence2vec](https://cs.stanford.edu/~quocle/paragraph_vector.pdf)