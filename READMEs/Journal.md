# Tuning parameters

For this phase i will be looking at the *Analogy task accurancy* with 100001 steps.

Moreover I'll be using the filter option with the following True parameters:
- remove_stopwords
- remove_punctuation
- deep_clean
- min_char=2

Finally I'll be using the *skipgram* model, and building the dictionary using the most_common method (by_freq=False) with 
100 words from all the domains (domain_words=100)

## Batch Variables

### skip_window
- changing from 1 to 3 - accurancy went up to 1.6 from 0
- changing from 3 to 4 - still 1.6

### batch_size
- 128->526, still 1.6

### num_skips
- 2->4, no difference

###drop by frequencies
Accurancy drops too, from 1.6 to 0.4

## Optimizer
- changing from GradientDescentOptimizer to AdamOptimaizer
- learning rate 1->0.01
Way slower, accurancy didn't grow


# SKIPGRAM VS CBOW
- Cbow seems much more precise than skipgram, but i was using the sampled_softmax loss so now i'll try using the nce loss for the
cbow too and see what i'll get. Nce loss seems slightly more preciese than softmax.


- while training the word2vec i notice that the accurancy rises with steps, it goes up by circa 1.5% every 100k iterations, on the other
hand the loss reaces a value 2.5 at it's 1M iteration and stays almost contast



# TODO

## Word2Vec
- [X] Fix tensorboard (split logs form models)
- [X] Fix accurancy
- [X] Compare different kind of loss
- [X] Use both skipgram and cbow for comparison
- [X] add accurancy as tf varible

## Classificator

###X/Y Train
-[X] Get a list of sublists, where each elem of the sublist is a document form a domain,
and each elem from the list is all the documents in a specific domain
-[X] define a dictionary with key:word value: embedded word
-[X] Encode each category into an int
-[] Crate a matrix in which a column is a domain and a row is a document from that domain
-[] (OPTIONAL) Cut the rows for the minimum number of documents (to have a regular matrix)
-[] For each cell use remove everything which is not in the metadata
-[] For each word in each cell convert the word with the embedding using the metadata

### Init graph
- Create x,y place holders
- crate a list of tf.contrib.rnn.BasicRNNCell
- create the same list but appliyng dropout (cells_drop=[tf.contrib.rnn.DropoutWrapper()])
- put the list in a tf.contrin.rnn.MutliRNNCell
- create the tf.nn.dynamic_rnn object and get the output,states
- create the tf.layers.dense
- coose loss function (a kind of softmax) and reduce the mean
- choose an optimizer (adamOptimizer)
- minimize the optimizer loss
- get the accurancy with reduce_mean(tf.nn.in_top_k)
- init the global variables

### trainig
- create a session
- run the init
- start the for epoch loop
- start the iteration loop
- define a batch function
- get x/y_batch from the batch function
- reshape the x_train
- run the session
- exit iteration loop
- evaluate accurancy

### Configurations
- Use another configuration class for the classifier that uses some parameters of the other one
