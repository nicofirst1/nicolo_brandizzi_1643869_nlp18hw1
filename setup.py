#!/usr/bin/env python
from pip.req import parse_requirements
from distutils.core import setup


with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='nlp18hw1',
      version='1.0',
      description='First Homework of NLP course',
      author='Nicolò Brandizzi',
      author_email='brandizzi.1643869@studenti.uniroma1.it',
      install_requires=requirements,
      )