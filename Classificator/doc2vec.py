from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import SGDClassifier, PassiveAggressiveClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
import _pickle as cPickle
from HomeWork1.Utils.data_analysis import pred_infos

from HomeWork1.Classificator.doc2vec_utils import *
from HomeWork1.Utils.utils import beep, recover_key

# Defining global varaibles
words = -1
model = 5
valid_size = 0
max_train_instances = 1
max_test_instances = 1
save = False
MAIN_DIR = "Classificator/pickles/"
train_pickle_dir = MAIN_DIR + "train_pickle/"

# ==== Reading the training dataset
try:
    print("Reading pickles")
    with open(train_pickle_dir + "train.pickle", "rb") as file:
        train = cPickle.load(file)

    print("Reading pickles")

    with open(train_pickle_dir + "embed.pickle", "rb") as file:
        embed_dict = cPickle.load(file)

    valid = 0

except FileNotFoundError:

    # get the trainig data and slipt it
    train_data, _, embed_dict = read_dataset(model, "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/TRAIN",
                                             domain_words=words)
    print("")
    train, label_dict, valid = split_XYdata(train_data, valid_set=valid_size)

    with open(train_pickle_dir + "train.pickle", "wb") as file:
        cPickle.dump(train, file)

    with open(train_pickle_dir + "embed.pickle", "wb") as file:
        cPickle.dump(embed_dict, file)

# ==== Building tha x/y train plus the validation set if valid_size>0
# print(label_dict)
x_train = train[0]
y_train = train[1]

max_train_instances = round(len(x_train) * max_train_instances)

x_train = x_train[:max_train_instances]
y_train = y_train[:max_train_instances]

# save memory
del train

# ==== Initialize the classificators
knc = KNeighborsClassifier(n_jobs=-1, weights='distance', n_neighbors=7)
svm = SVC(probability=True, random_state=42, decision_function_shape='ovr', shrinking=True, C=200, verbose=1)
rfc = RandomForestClassifier(n_jobs=-1, random_state=42, criterion='gini', n_estimators=500, oob_score=True, verbose=1)
sgd = SGDClassifier(n_jobs=-1, loss='log', learning_rate='optimal', verbose=1)
mlp = MLPClassifier(beta_1=0.8, activation='logistic', solver="adam", hidden_layer_sizes=50, learning_rate='adaptive',
                    tol=0.000001, max_iter=300)
stack = VotingClassifier(n_jobs=1, estimators=[('knc', knc), ('rfc', rfc)], voting='soft')
pac = PassiveAggressiveClassifier(n_jobs=-1, C=0.1, fit_intercept=False, loss='hinge')

# choosing one classificator
classificator = mlp

# ===== Building the validation dataset and computing the gridsearch for optimal parameters
if valid_size:
    valid_size = round(len(x_train) * valid_size)

    x_valid = x_train[:valid_size]
    y_valid = y_train[:valid_size]

    # save memory
    del valid

    # initialize the grid search parameters for each classificator
    grid_dict_knc = {"n_neighbors": [6, 7, 8, 10]}
    grid_dict_svc = {"C": [100, 200, 300, 400, 500]}
    grid_dict_rfc = {"oob_score": [True, False], }  # 'bootstrap':[True,False]}
    grid_dict_abc = {'n_estimators': [100, 200, 300], 'learning_rate': [0.01, 0.001, 0.0001]}
    grid_dict_mlp = {'solver': ['lbfgs', 'sgd', 'adam']}
    grid_dict_gpc = {'learning_rate': ['constant', 'invscaling', 'adaptive']}
    grid_dict_pac = {'loss': ['hinge', 'squared_hinge']}

    # initializing the grid search object
    rand = GridSearchCV(classificator, grid_dict_mlp, verbose=1, n_jobs=-1)

    # fitting the data
    rand.fit(x_train, y_train)

    # getting the best results
    best_e = rand.best_estimator_
    best_p = rand.best_params_
    best_s = rand.best_score_

    # print the best estimator parameters
    print(f"Best params {best_p}")
    print(f"Best score {best_s}")

    # begin prediction on validation set
    y_pred = best_e.predict(x_valid)
    pred_infos(y_pred, y_valid, confusion=False)

    # save memory
    del x_valid, y_valid

    # choose the best classificator as the current one
    classificator = best_e

# ==== Training
print("Training")
classificator.fit(x_train, y_train)

#save memory
del x_train, y_train


# ==== Read the test dataset
test_pickle_dir = MAIN_DIR + "test_pickle/"

try:
    with open(test_pickle_dir + "test.pickle", "rb") as file:
        test = cPickle.load(file)

    print("Reading pickles")

    with open(test_pickle_dir + "label.pickle", "rb") as file:
        label_dict = cPickle.load(file)

    print("Reading pickles")

    with open(test_pickle_dir + "files.pickle", "rb") as file:
        files = cPickle.load(file)



except FileNotFoundError:

    # get the test data
    test_data, files, _ = read_dataset(model, "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/DEV",
                                       domain_words=words)
    print("")
    test, label_dict, _ = split_XYdata(test_data)

    with open(test_pickle_dir + "test.pickle", "wb") as file:
        cPickle.dump(test, file)

    with open(test_pickle_dir + "label.pickle", "wb") as file:
        cPickle.dump(label_dict, file)

    with open(test_pickle_dir + "files.pickle", "wb") as file:
        cPickle.dump(files, file)

# build the test dataset
print("Predicting")
x_test = test[0]
y_test = test[1]

max_test_instances = round(len(x_test) * max_test_instances)

x_test = x_test[:max_test_instances]
y_test = y_test[:max_test_instances]

# predict on test
y_pred = classificator.predict(x_test)

del x_test

print("Saving prediction")
if save:
    with open("test_answers.tsv", "w+") as file:
        for f, answer in zip(files, y_pred):
            id_ = f.split(".")[0]
            pred = recover_key(label_dict, answer)
            file.write(f"{id_}\t{pred}\n")

# get prediction infos
pred_infos(y_pred, y_test, confusion=False)

beep()
