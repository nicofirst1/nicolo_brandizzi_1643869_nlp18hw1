from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import SGDClassifier, PassiveAggressiveClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
import _pickle as cPickle
from HomeWork1.Utils.data_analysis import pred_infos

from HomeWork1.Classificator.doc2vec_utils import *
from HomeWork1.Utils.utils import beep, recover_key

# Defining global varaibles
words = -1
model = 5
max_train_instances = 1
MAIN_DIR = "pickles/"
train_pickle_dir = MAIN_DIR + "train_pickle/"

# ==== Reading the training dataset
try:
    print("Reading pickles")
    with open(train_pickle_dir + "train.pickle", "rb") as file:
        train = cPickle.load(file)

    print("Reading pickles")

    with open(train_pickle_dir + "embed.pickle", "rb") as file:
        embed_dict = cPickle.load(file)

    with open(train_pickle_dir + "label_dict.pickle", "rb") as file:
        label_dict = cPickle.load(file)

    valid = 0

except FileNotFoundError:

    # get the trainig data and slipt it
    train_data, _, embed_dict = read_dataset(model, "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/TRAIN",
                                             domain_words=words)
    print("")
    train, label_dict, valid = split_XYdata(train_data)

    with open(train_pickle_dir + "train.pickle", "wb") as file:
        cPickle.dump(train, file)

    with open(train_pickle_dir + "embed.pickle", "wb") as file:
        cPickle.dump(embed_dict, file)

    with open(train_pickle_dir + "label_dict.pickle", "wb") as file:
        cPickle.dump(label_dict, file)

# ==== Building tha x/y train plus the validation set if valid_size>0
# print(label_dict)
x_train = train[0]
y_train = train[1]

max_train_instances = round(len(x_train) * max_train_instances)

x_train = x_train[:max_train_instances]
y_train = y_train[:max_train_instances]

# save memory
del train

mlp = MLPClassifier(beta_1=0.8, activation='logistic', solver="adam", hidden_layer_sizes=50, learning_rate='adaptive',
                    tol=0.000001, max_iter=300)

# choosing one classificator
classificator = mlp


# ==== Training
print("Training")
classificator.fit(x_train, y_train)

#save memory
del x_train, y_train


# get the list of files
docs = os.listdir("/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/TEST")
docs=[os.path.join("/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/TEST/",elem) for elem in docs]



y_pred = []
batch_size=100
len_docs=len(docs)
info_step=1000
idx=0
# predicting
for idx in range(batch_size, len_docs, batch_size):


    # read a list of sentences
    files = read_files(docs[idx - batch_size:idx],embed_dict, True)
    preds=classificator.predict(files)


    y_pred+=list(preds)

    print(f"\rbatch {idx}/{len_docs}",end="")


#read the remaning files
files_left=len_docs-idx
print(f"\nfile_left:{files_left}, idx:{idx}")
# read a list of sentences
files = read_files(docs[-files_left:], embed_dict, True)

preds = classificator.predict(files)

y_pred += list(preds)


#save the answers to file
with open("test_answers.tsv","w+") as file:
     for f,answer in zip(docs,y_pred):
         file_id=f.split(".")[0].split("/")[-1].split("_")[1].split(".")[0]
         pred=recover_key(label_dict,answer)
         file.write(f"{file_id}\t{pred}\n")


beep()
