import os
import shutil
from asyncio import sleep
import pickle
import random
from multiprocessing.pool import Pool

import numpy as np
from nltk import sent_tokenize
from sklearn.decomposition import PCA
from termcolor import colored
from functools import reduce
from HomeWork1.Word2Vec.data_processing import filter_document


def read_dictionary(path2dir, words):
    # load the dict with picle
    with open(path2dir, "rb") as file:
        dictionary = pickle.load(file)
        # generate reverse_dict and train_data
        reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
        data = []
        for word in words:
            index = dictionary.get(word, 0)
            data.append(index)

        print(colored("...train_data loaded from pickle dictionry", color="blue"))
        return data, dictionary, reverse_dictionary


def generate_dict(model_meta, model_embed):
    """Generate the dictionary in which key:word and value: corresponding embedding"""

    print("Generating embedding dictionary")

    with open(model_meta, "r+") as meta_f:
        meta_l = meta_f.readlines()
        meta_l = [elem.strip("\n") for elem in meta_l]

    with open(model_embed, "rb") as embed_f:
        embed_l = pickle.load(embed_f)

    dictionary = {}
    for w, e in zip(meta_l, embed_l):
        dictionary[w] = e

    return dictionary



def read_files(files,domains,filter_,min_sentnece_len=5,max_sentence_per_doc=-1):

    data=[]

    to_read=len(files)
    idx=0

    for f in files:
        with open(f) as file:
            limit=max_sentence_per_doc
            data_sentences = []  # a sentence for each line
            # for every line in the file
            for line in file.readlines():

                # split the line into sentences
                sentences = sent_tokenize(line)

                if limit<len(sentences) and limit!=-1:
                    sentences=sentences[:limit]
                    limit=0

                else: limit-=len(sentences)

                for sentence in sentences:


                    # split the line by words
                    split = sentence.lower().strip().split()


                    if filter_:
                        split = filter_document(split)

                    if len(split)<min_sentnece_len:continue

                    data_sentences.append(split)


                # clean train_data sentence
                data_sentences = [elem for elem in data_sentences if elem]

                if limit==0: break

            data.append((domains[idx],data_sentences))
            # clean train_data document
            data = [elem for elem in data if elem[1]]
        idx+=1
        #print(f"\rReading file {idx}/{to_read}",end="")

    return data

def read_documents(directory, domain_words, filter_):
    """
       Read the train_data form the direcotry and split it by domain
       :param directory:  the direcotry in which the train_data is stored
       :return: a dictionary in which the key is the domain name and the value:
            a list of words
                in a list of sentences
                    in a list of documents
       """

    data = {}  # a dictionary with
    to_read = len(os.listdir(directory))
    idx = 0
    # read the train_data from all domains

    for domain in os.listdir(directory):
        # determine a limit for each domain
        limit = domain_words
        data[domain] = []
        data_domain = []

        # shuffle the files in each domain
        files = os.listdir(os.path.join(directory, domain))
        random.shuffle(files)
        for f in files:
            data_document = []
            # if the limit for the domain has been reached pass to the next one
            if limit == 0: break

            if f.endswith(".txt"):
                # read the train_data form a specific document
                with open(os.path.join(directory, domain, f)) as file:

                    data_sentences = []  # a sentence for each line
                    # for every line in the file
                    for line in file.readlines():

                        # split the line into sentences
                        sentences = sent_tokenize(line)

                        for sentence in sentences:

                            # split the line by words
                            split = sentence.lower().strip().split()

                            if filter_:
                                split = filter_document(split)

                            # if the limit has been reached
                            if limit > 0 and limit - len(split) < 0:
                                # take just the words which does not exceed the limit
                                split = split[:limit]
                                # add them to the train_data
                                data_sentences.append(split)
                                # set the limit to zero to break
                                limit = 0
                            # else update decrease the limit
                            elif limit > 0:
                                limit -= len(split)

                            # if the limit hasn't been reached yet
                            if limit > 0 or limit == -1:
                                data_sentences.append(split)
                            # if the limit has been reached exit
                            if limit == 0:
                                break
                        # clean train_data sentence
                        data_sentences = [elem for elem in data_sentences if elem]

                    data_document += data_sentences
                    # clean train_data document
                    data_document = [elem for elem in data_document if elem]

            data_domain.append(data_document)
            # clean train_data domain
            data_domain = [elem for elem in data_domain if elem]
        data[domain] += data_domain

        idx += 1
        print(colored(f"\rReading file {idx}/{to_read}", color="cyan"), end="")

    print("")
    return data


def read_dataset(model, data_dir, domain_words=200, test=False, min_sentence_len=8,max_sentences_per_domain=200):
    """
    Build the dataset form a specific model
    :param min_sentence_len: the minimum number of words in a sentence to be considered
    :param model: the model into 'Good_models' to use
    :param data_dir: the data direcotry
    :param domain_words: the maximum domain words to read
    :param test:
    :return: data as a dictionary in which:
        key: is the domain integer
        value: is a list of arrays, each element of the list is  sentence from that specific domain
    """

    # get the embed dict
    metadata = f"../Good_models/Model_{model}/Model/metadata.tsv"
    embed = f"../Good_models/Model_{model}/Operators/embed.pickle"
    embed_dict = generate_dict(metadata, embed)

    # read the sentences form the data dir
    if not test:
        data = read_documents_sentences(data_dir, domain_words, min_sentence_len=min_sentence_len,max_sentences_per_domain=max_sentences_per_domain)
        # transform words to embed vectors
        data = {w: sentences2embed(embed_dict, v) for w, v in data.items()}
    else:
        data = read_documents(data_dir, domain_words, True)
        data = {w: [sentences2embed(embed_dict, elem) for elem in v] for w, v in data.items()}

    # save memory
    del embed_dict

    # vectoralize train_data
    if not test:
        data = vectorize_train(data)
    else:
        data = vectorize_test(data)

    return data


def split_train_data(data, valid_set=0.0):
    """
    Split the train_data into x,y vectors
    :param data: dictionary where key is the y and value is a list of samples
    :param valid_set: the dimension of the valid set in perc
    :returns:
        train: a tuple of two 1D array (sample, label)
        y_label_dict: a dictionary which maps a train_data key to an int
        y_train:  a tuple of two 1D array (sample, label)

    """

    print("Splitting X/Y Train")
    y_label_dict = {}

    for k in data.keys():
        y_label_dict[k] = len(y_label_dict)

    train_set = []
    for k, v in data.items():
        for d in v:
            train_set.append((y_label_dict[k], d))

    # print("Saving train_data")
    # with open("train_data.json","wb") as file:
    #     ujson.dump(train_set,file)

    random.shuffle(train_set)

    y_train = [y for y, _ in train_set]
    x_train = [x for _, x in train_set]
    x_train = np.array(x_train)

    valid_size = round(len(x_train) * valid_set)
    x_valid = x_train[:valid_size]
    y_valid = y_train[:valid_size]
    x_train = x_train[valid_size + 1:]
    y_train = y_train[valid_size + 1:]
    x_train = np.array(x_train)

    train = (x_train, y_train)
    valid = (x_valid, y_valid)

    return train, y_label_dict, valid


def split_test_data(data, valid_set=0.0):
    """
    Split the train_data into x,y vectors
    :param data: dictionary where key is the y and value is a list of samples
    :param valid_set: the dimension of the valid set in perc
    :returns:
        train: a tuple of two 1D array (sample, label)
        y_label_dict: a dictionary which maps a train_data key to an int
        y_train:  a tuple of two 1D array (sample, label)

    """

    print("Splitting X/Y Train")
    y_label_dict = {}

    for k in data.keys():
        y_label_dict[k] = len(y_label_dict)

    train_set = []
    for k, v in data.items():
        for d in v:
            train_set.append((y_label_dict[k], d))

    # print("Saving train_data")
    # with open("train_data.json","wb") as file:
    #     ujson.dump(train_set,file)

    random.shuffle(train_set)

    y_train = [y for y, _ in train_set]
    x_train = [x for _, x in train_set]
    x_train = np.array(x_train)

    valid_size = round(len(x_train) * valid_set)
    x_valid = x_train[:valid_size]
    y_valid = y_train[:valid_size]
    x_train = x_train[valid_size + 1:]
    y_train = y_train[valid_size + 1:]
    x_train = np.array(x_train)

    train = (x_train, y_train)
    valid = (x_valid, y_valid)

    return train, y_label_dict, valid


def sentences2embed(dictionary, sentences):
    """
    Embed a list of sentences
    :param dictionary: the dictionary used for word to embed conversion
    :param sentences: a list of sentences which is a list of list of words
    :return: a vector of embedded words in a list of words in a list of sentnces
    """

    found = 0
    total = 0

    embed_sentences = []

    for sentence in sentences:

        embed_words = []

        for word in sentence:
            try:
                word = dictionary[word]
                found += 1
            except KeyError:
                word = dictionary['UNK']

            embed_words.append(word)
            total += 1

        embed_sentences.append(embed_words)

    # print(f"\rWords found {found/total*100}%",end="")

    return embed_sentences


def write_dictionary(dictionary):
    with open("vocab.txt", "w+") as file:
        for k, v in dictionary.items():
            file.write(f"{k} {v}\n")


def read_documents_sentences(directory, domain_words, min_sentence_len=4,max_sentences_per_domain=200):
    """
       Read the train_data form the direcotry and split it by domain
       :param directory:  the direcotry in which the train_data is stored
       :return: a dictionary in which the key is the domain name and the value:
            a list of words
                in a list of sentences
                    in a list of documents
       """

    data = {}  # a dictionary with
    to_read = len(os.listdir(directory))
    idx = 0
    # read the train_data from all domains

    for domain in os.listdir(directory):
        # determine a limit for each domain
        limit = domain_words
        data[domain] = []

        sent_limit=max_sentences_per_domain

        # shuffle the files in each domain
        files = os.listdir(os.path.join(directory, domain))
        random.shuffle(files)
        for f in files:
            data_document = []
            # if the limit for the domain has been reached pass to the next one
            if limit == 0: break
            if sent_limit == 0: break

            if f.endswith(".txt"):
                # read the train_data form a specific document
                with open(os.path.join(directory, domain, f)) as file:

                    data_sentences = []  # a sentence for each line
                    # for every line in the file
                    for line in file.readlines():

                        # split the line into sentences
                        sentences = sent_tokenize(line)

                        if sent_limit<len(sentences):
                            sentences=sentences[:sent_limit]
                            sent_limit=0

                        else:
                            sent_limit-=len(sentences)


                        for sentence in sentences:

                            # split the line by words
                            split = sentence.lower().strip().split()

                            split = filter_document(split)

                            if len(split) < min_sentence_len:
                                continue

                            # if the limit has been reached
                            if limit > 0 and limit - len(split) < 0:
                                # take just the words which does not exceed the limit
                                split = split[:limit]
                                # add them to the train_data
                                data_sentences.append(split)
                                # set the limit to zero to break
                                limit = 0
                            # else update decrease the limit
                            elif limit > 0:
                                limit -= len(split)

                            # if the limit hasn't been reached yet
                            if limit > 0 or limit == -1:
                                data_sentences.append(split)
                            # if the limit has been reached exit
                            if limit == 0:
                                break

                        # clean train_data sentence
                        data_sentences = [elem for elem in data_sentences if elem]

                    if sent_limit == 0:
                        break
                    data_document += data_sentences
                    # clean train_data document
                    data_document = [elem for elem in data_document if elem]
            data[domain] += data_document

        idx += 1
        print(colored(f"\rReading file {idx}/{to_read}", color="cyan"), end="")

    print("")
    return data


def get_files(directory,max_doc_per_domain=-1):
    """
    Get a list of all the files in the data direcotry
    :param directory: the direcotry in which to look in for files
    :return: a list of tuples, (domain name, list of path to files)
    """

    ret_files = []

    for domain in os.listdir(directory):
        limit=max_doc_per_domain

        for f in  os.listdir(os.path.join(directory, domain)):

            if f.endswith(".txt"):
                # read the train_data form a specific document
                ret_files.append((domain,os.path.join(directory, domain, f)))
                limit-=1

                if limit==0: break

    return ret_files


def parallel_vectorize(sentences):
    mean_s = []

    for sentence in sentences:
        mean_s.append(np.mean(sentence, axis=0))

    return mean_s


def parallel_vectorize_sentences(sentences):
    return np.mean(sentences, axis=0)


def vectorize_train(data):
    """
    Transform every elem of the train_data into a vector
    :param data: dictionary, where the key is a domain and the value is a list of documents
                composed by a list of sentences
                composed by a list of words
                which is a vector
    :return: a dictionary in which the key is the domain and the value a vector
    """

    new_dict = {}

    total = len(data)
    idx = 1

    # create a pool
    pool = Pool(10)

    for domain, documents in data.items():
        new_dict[domain] = pool.map(parallel_vectorize_sentences, documents)

        print(f"\rVecotrized {idx}/{total}", end="")
        idx += 1

    return new_dict

def vectorize_batch(sentences,embed_dict):
    """
    Transform every elem of the train_data into a vector
    :param data: dictionary, where the key is a domain and the value is a list of documents
                composed by a list of sentences
                composed by a list of words
                which is a vector
    :return: a dictionary in which the key is the domain and the value a vector
    """

    x_train=[]
    y_train=[]

    total = len(sentences)
    idx = 1

    for domain,sentence in sentences:
        embed_words=[]

        for words in sentence:

            for word in words:
                try:
                    word = embed_dict[word]
                except KeyError:
                    word = embed_dict['UNK']


                embed_words.append(word)

        x_train.append(np.mean(embed_words,axis=0))
        y_train.append(domain)

        #print(f"\rvectorizing {idx}/{total}",end="")
        idx+=1

    print("")

    return x_train,y_train



def vectorize_test(data):
    """
    Transform every elem of the train_data into a vector
    :param data: dictionary, where the key is a domain and the value is a list of documents
                composed by a list of sentences
                composed by a list of words
                which is a vector
    :return: a dictionary in which the key is the domain and the value a vector
    """

    new_dict = {}

    total = len(data)
    idx = 1

    # create a pool
    pool = Pool(10)

    for domain, documents in data.items():
        new_dict[domain] = pool.map(parallel_vectorize, documents)

        print(f"\rVecotrized {idx}/{total}", end="")
        idx += 1

    return new_dict


def vectorize_old(data):
    """
    Transform every elem of the train_data into a vector
    :param data: dictionary, where the key is a domain and the value is a list of documents
                composed by a list of sentences
                composed by a list of words
                which is a vector
    :return: a dictionary in which the key is the domain and the value a vector
    """

    new_dict = {}

    total = len(data)
    idx = 1

    for domain, documents in data.items():

        mean_d = []

        for document in documents:

            mean_ss = []

            for sentences in document:

                mean_s = []

                for sentence in sentences:
                    mean_s.append(np.mean(sentence, axis=0))

                mean_ss.append(np.mean(mean_s, axis=0))

            mean_d.append(np.mean(mean_ss, axis=0))

        new_dict[domain] = mean_d
        print(f"\rVectorialized {idx}/{total}", end="")
        idx += 1

    print()
    return new_dict


def parallel_reading(f):
    """
    Perform words cleaning on a single file
    :param f: the file on which the cleaning is to be performed
    :return: a list of words from the file
    """

    parall_data = []

    # read the train_data form all documents in domain
    if f.endswith(".txt"):
        # read the train_data form a specific document
        with open(f) as file:
            # for every line in the file
            for line in file.readlines():
                # split the line by words
                split = line.lower().strip().split()
                parall_data += filter_document(split)

    return parall_data


def read_documents_parallel(directory):
    """
    Read the train_data
    :param directory:  the direcotry in which the train_data is stored
    :return: a list of words from all the train_data
    """

    data = {}
    to_read = len(os.listdir(directory))
    idx = 0

    # create a pool
    pool = Pool(10)

    # for every domain in the dataset
    for domain in os.listdir(directory):
        # get the domain path
        path = os.listdir(os.path.join(directory, domain))
        path = [os.path.join(f"{directory}/{domain}", elem) for elem in path]
        data[domain] = pool.map(parallel_reading, path)
        idx += 1
        print(f"\rReading domain {domain}: {idx}/{to_read}", end="")

    print("")
    return data


def write_data(data, file_path):
    """write al list of tuple to a file usign the style elem[0]:elem[1]
    :param data: list of tuples"""

    with open(file_path, "w+") as file:
        for c, v in data:
            file.write(f"{c}:{v}\n")
