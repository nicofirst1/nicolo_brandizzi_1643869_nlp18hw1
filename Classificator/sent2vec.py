from collections import Counter

from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from HomeWork1.Utils.data_analysis import pred_infos
from HomeWork1.Classificator.sent2vec_util import *

# choose the model to use (in good models) and the words to read from each dir
from HomeWork1.Utils.utils import beep

words = 300000
model = 5
valid_size=0
max_test_len=300

#read the data as a dictionary
train_data = read_dataset(model, "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/TRAIN", domain_words=words)
print("")
train, label_dict, valid = split_train_data(train_data, valid_set=valid_size)

print(label_dict)
x_train = train[0]
y_train = train[1]

del train,train_data


# initialize the classificator
knc = KNeighborsClassifier(n_jobs=-1, weights='distance', n_neighbors=7)
svm =SVC(probability=True,random_state=42,decision_function_shape='ovr',shrinking=True,C=200)
rfc=RandomForestClassifier(n_jobs=-1,random_state=42,criterion='gini',n_estimators=500,oob_score=True)
sgd=SGDClassifier(n_jobs=-1,loss='log',eta0=0.1,learning_rate='optimal')


stack = VotingClassifier(n_jobs=-1,estimators=[ ('knc', knc),('rfc', rfc)], voting='soft')
classificator=knc

if valid_size:

    x_valid = valid[0]
    y_valid = valid[1]

    del valid

    grid_dict_knc = {"n_neighbors": [ 10,5]}
    grid_dict_svc = {"C": [100,200,300,400,500]}
    grid_dict_rfc = {"oob_score": [True,False],}#'bootstrap':[True,False]}
    grid_dict_abc={'n_estimators':[100,200,300],'learning_rate':[0.01,0.001,0.0001]}
    grid_dict_sgd={'alpha':[0.0001,0.00001,0.000001],'learning_rate':['constant','optimal','invscaling']}

    rand = GridSearchCV(classificator, grid_dict_sgd, verbose=1, n_jobs=-1)

    rand.fit(x_train, y_train)

    best_e = rand.best_estimator_
    best_p = rand.best_params_
    best_s = rand.best_score_

    print(f"Best params {best_p}")
    print(f"Best score {best_s}")

    y_pred = best_e.predict(x_valid)
    pred_infos(y_pred, y_valid,confusion=False)

    del x_valid,y_valid

    classificator=best_e



# train
print("Training")
classificator.fit(x_train, y_train)

del x_train, y_train


# get the test data
test_data = read_dataset(model, "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/DEV", domain_words=words,test=True)
print("")
test, _, _ = split_test_data(test_data)

del test_data

y_test = test[1]
x_test=test[0]

del test

if len(x_test)>max_test_len:
    x_test=x_test[:max_test_len]
    y_test=y_test[:max_test_len]
#the x_test is a list, each element is as a document which has multiple sentences as arrays
y_pred=[]

to_predict=len(x_test)
idx=0
accuracy=0

for  document, domain in zip(x_test,y_test):
    pred=Counter(classificator.predict(document)).most_common()
    y_pred.append(pred[0][0])

    try:
        accuracy=round(sum(1 for r,p in zip(y_pred,y_test[:idx]) if r==p)/idx*100,2)
    except ZeroDivisionError:
        accuracy=sum(1 for r,p in zip(y_pred,y_test[:idx]) if r==p)/1*100


    print(f"\rPredicting {idx}/{to_predict}, accuracy={accuracy}",end="")
    idx+=1

print("")


# get prediction infos
pred_infos(y_pred, y_test)

beep()
