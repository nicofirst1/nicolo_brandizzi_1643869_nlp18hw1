from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
import _pickle as cPickle
from HomeWork1.Classificator.sent2vec_util import get_files
from HomeWork1.Utils.data_analysis import pred_infos

from HomeWork1.Classificator.doc2vec_utils import *
from HomeWork1.Utils.utils import beep, recover_key

#Defining global varaibles
words = -1
model = 5
valid_size=0

pickle_dir="pickle/"

try:
    print("Reading pickles")
    with open(pickle_dir+"train.pickle", "rb") as file:
        train=cPickle.load(file)

    print("Reading pickles")

    with open(pickle_dir+"embed.pickle", "rb") as file:
        embed_dict=cPickle.load(file)

    print("Reading pickles")


    with open(pickle_dir+"label.pickle", "rb") as file:
        label_dict=cPickle.load(file)

    valid=0

except FileNotFoundError:

    # get the trainig data and slipt it
    train_data,_,embed_dict = read_dataset(model, "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/TRAIN", domain_words=words)
    print("")
    train, label_dict, valid = split_XYdata(train_data,valid_set=valid_size)

    with open(pickle_dir+"train.pickle","wb") as file:
        cPickle.dump(train,file)

    with open(pickle_dir+"embed.pickle","wb") as file:
        cPickle.dump(embed_dict,file)

    with open(pickle_dir+"label.pickle","wb") as file:
        cPickle.dump(embed_dict,file)


#print(label_dict)
x_train = train[0]
y_train = train[1]

del train


# initialize the classificator
knc = KNeighborsClassifier(n_jobs=-1, weights='distance', n_neighbors=7)
svm =SVC(probability=True,random_state=42,decision_function_shape='ovr',shrinking=True,C=200)
rfc=RandomForestClassifier(n_jobs=-1,random_state=42,criterion='gini',n_estimators=500,oob_score=True)
sgd=SGDClassifier(n_jobs=-1,loss='log',learning_rate='optimal')


stack = VotingClassifier(n_jobs=-1,estimators=[ ('knc', knc),('rfc', rfc)], voting='soft')
classificator=knc

if valid_size:

    x_valid = valid[0]
    y_valid = valid[1]

    del valid

    grid_dict_knc = {"n_neighbors": [6, 7, 8, 10], "algorithm": ['auto', 'kd_tree', 'ball_tree']}
    grid_dict_svc = {"C": [100,200,300,400,500]}
    grid_dict_rfc = {"oob_score": [True,False],}#'bootstrap':[True,False]}
    grid_dict_abc={'n_estimators':[100,200,300],'learning_rate':[0.01,0.001,0.0001]}

    rand = GridSearchCV(classificator, grid_dict_abc, verbose=1, n_jobs=-1)

    rand.fit(x_train, y_train)

    best_e = rand.best_estimator_
    best_p = rand.best_params_
    best_s = rand.best_score_

    print(f"Best params {best_p}")
    print(f"Best score {best_s}")

    y_pred = best_e.predict(x_valid)
    pred_infos(y_pred, y_valid)

    del x_valid,y_valid

    classificator=best_e



# train
print("Training")
classificator.fit(x_train, y_train)

del x_train, y_train

#
#
# # get the test data
# test_data,files = read_dataset(model, "/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/DEV",domain_words=words)
# print("")
# test, label_dict, _ = split_XYdata(test_data)
#
# x_test = test[0]
# y_test = test[1]
#
# # predict on test
# y_pred = classificator.predict(x_test)
#
# del x_test
#
# with open("test_answers.tsv","w+") as file:
#     for f,answer in zip(files,y_pred):
#         id_=f.split(".")[0]
#         pred=recover_key(label_dict,answer)
#         file.write(f"{id_}\t{pred}\n")
#
# # get prediction infos
# pred_infos(y_pred, y_test,confusion=False)
#
# beep()
#



# get the list of files
documents = get_files("/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/DEV")

random.shuffle(documents)

domains = [d for d, _ in documents]
domain_labeler = {}
for d in set(domains):
    domain_labeler[d] = len(domain_labeler)

domains = [domain_labeler[d] for d in domains]

docs = [d for _, d in documents]

del documents

accuracy = 0
y_pred = []
batch_size=100
len_docs=len(docs)
info_step=1000
idx=0
# predicting
for idx in range(batch_size, len_docs, batch_size):


    # read a list of sentences
    files = read_files(docs[idx - batch_size:idx],embed_dict, True)
    preds=classificator.predict(files)


    y_pred+=list(preds)

    print(f"\rbatch {idx}/{len_docs}",end="")

    if idx%info_step==0:

        try:
            accuracy = round(sum(1 for r, p in zip(y_pred, domains[:idx]) if r == p) / len(y_pred) * 100, 2)
        except ZeroDivisionError:
            accuracy = sum(1 for r, p in zip(y_pred,  domains[:idx]) if r == p) / 1 * 100

        finally:
            total=len([1 for r, p in zip(y_pred,  domains[:idx]) if r == p])

        print(f"\naccuracy={accuracy}, right/total={total}/{len(y_pred)}")

files_left=len_docs-idx
print(f"\nfile_left:{files_left}, idx:{idx}")
# read a list of sentences
files = read_files(docs[-files_left:], embed_dict, True)

preds = classificator.predict(files)

y_pred += list(preds)

pred_infos(y_pred,domains)

with open("test_answers.tsv","w+") as file:
     for f,answer in zip(docs,y_pred):
         id_=f.split(".")[0].split("/")[-1]
         pred=recover_key(label_dict,answer)
         file.write(f"{id_}\t{pred}\n")


beep()
