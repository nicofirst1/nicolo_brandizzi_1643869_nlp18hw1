from collections import Counter

from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from HomeWork1.Classificator.sent2vec_util import *

# choose the model to use (in good models) and the words to read from each dir
from HomeWork1.Utils.utils import beep

model = 5
valid_size = 0
max_test_len = 200
max_doc_per_domain = 3000
max_sentence_per_doc = 10000
batch_size = 65000  # documents

# =======================TRAINING=================================

# get the list of files
documents = get_files("/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/TRAIN",
                      max_doc_per_domain=max_doc_per_domain)

random.shuffle(documents)

domains = [d for d, _ in documents]
domain_labeler = {}
for d in set(domains):
    domain_labeler[d] = len(domain_labeler)

domains = [domain_labeler[d] for d in domains]

docs = [d for _, d in documents]

del documents
# get the embed dict
metadata = f"../Good_models/Model_{model}/Model/metadata.tsv"
embed = f"../Good_models/Model_{model}/Operators/embed.pickle"
embed_dict = generate_dict(metadata, embed)

knc = KNeighborsClassifier(n_jobs=-1, weights='distance', n_neighbors=7)
sgd = SGDClassifier(n_jobs=-1, loss='log', learning_rate='optimal')
classificator = sgd
print(len(docs))
# fitting
for idx in range(batch_size, len(docs), batch_size):
    y = domains[idx - batch_size:idx]

    # read a list of sentences
    sentences = read_files(docs[idx - batch_size:idx], y, True, max_sentence_per_doc=max_sentence_per_doc)
    x, y = vectorize_batch(sentences, embed_dict)
    classificator.fit(x, y)




    print(f"batch {idx}/{len(docs)}")

# =======================PREDICTION=================================


# get the list of files
documents = get_files("/home/dizzi/Work/PyCharm/NLP/DataSets/HW1/datasets/DATA/DEV")

random.shuffle(documents)

domains = [d for d, _ in documents]
domain_labeler = {}
for d in set(domains):
    domain_labeler[d] = len(domain_labeler)

domains = [domain_labeler[d] for d in domains]

docs = [d for _, d in documents]

del documents

accuracy = 0
y_pred = []
# predicting
for idx in range(batch_size, len(docs), batch_size):

    y = domains[idx - batch_size:idx]

    # read a list of sentences
    sentences = read_files(docs[idx - batch_size:idx], y, True)
    x, y = vectorize_batch(sentences, embed_dict)

    pred = Counter(classificator.predict(x)).most_common()
    y_pred.append(pred[0][0])

    print(f"batch {idx}/{len(docs)}")

    try:
        accuracy = round(sum(1 for r, p in zip(y_pred, y) if r == p) / len(y_pred) * 100, 2)
    except ZeroDivisionError:
        accuracy = sum(1 for r, p in zip(y_pred, y) if r == p) / 1 * 100

    print(f"\raccuracy={accuracy}", end="")

beep()
