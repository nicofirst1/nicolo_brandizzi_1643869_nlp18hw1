import os,random
from multiprocessing.pool import Pool

import numpy as np
from nltk import sent_tokenize
from termcolor import colored
import pickle


from HomeWork1.Word2Vec.data_processing import filter_document


def generate_dict(model_meta, model_embed):
    """Generate the dictionary in which key:word and value: corresponding embedding"""

    with open(model_meta, "r+") as meta_f:
        meta_l = meta_f.readlines()
        meta_l = [elem.strip("\n") for elem in meta_l]

    with open(model_embed, "rb") as embed_f:
        embed_l = pickle.load(embed_f)

    dictionary = {}
    for w, e in zip(meta_l, embed_l):
        dictionary[w] = e

    return dictionary


def read_documents(directory, domain_words, filter_):
    """
       Read the train_data form the direcotry and split it by domain
       :param directory:  the direcotry in which the train_data is stored
       :return: a dictionary in which the key is the domain name and the value:
            a list of words
                in a list of sentences
                    in a list of documents
       """

    data = {}  # a dictionary with
    to_read = len(os.listdir(directory))
    idx = 0
    read_files=[]
    # read the train_data from all domains

    for domain in os.listdir(directory):
        # determine a limit for each domain
        limit = domain_words
        data[domain] = []
        data_domain = []

        # shuffle the files in each domain
        files = os.listdir(os.path.join(directory, domain))
        random.shuffle(files)
        for f in files:
            data_document = []
            # if the limit for the domain has been reached pass to the next one
            if limit == 0: break

            if f.endswith(".txt"):
                read_files.append(f)
                # read the train_data form a specific document
                with open(os.path.join(directory, domain, f)) as file:

                    data_sentences = []  # a sentence for each line
                    # for every line in the file
                    for line in file.readlines():

                        # split the line into sentences
                        sentences = sent_tokenize(line)

                        for sentence in sentences:

                            # split the line by words
                            split = sentence.lower().strip().split()

                            if filter_:
                                split = filter_document(split)

                            # if the limit has been reached
                            if limit > 0 and limit - len(split) < 0:
                                # take just the words which does not exceed the limit
                                split = split[:limit]
                                # add them to the train_data
                                data_sentences.append(split)
                                # set the limit to zero to break
                                limit = 0
                            # else update decrease the limit
                            elif limit > 0:
                                limit -= len(split)

                            # if the limit hasn't been reached yet
                            if limit > 0 or limit == -1:
                                data_sentences.append(split)
                            # if the limit has been reached exit
                            if limit == 0:
                                break
                        # clean train_data sentence
                        data_sentences = [elem for elem in data_sentences if elem]

                    data_document += data_sentences
                    # clean train_data document
                    data_document = [elem for elem in data_document if elem]

            data_domain.append(data_document)
            # clean train_data domain
            data_domain = [elem for elem in data_domain if elem]
        data[domain] += data_domain

        idx += 1
        print(colored(f"\rReading file {idx}/{to_read}", color="cyan"), end="")

    print("")
    return data,read_files

def read_test_document(directory, filter_):
    """
       Read the train_data form the direcotry and split it by domain
       :param directory:  the direcotry in which the train_data is stored
       :return: a dictionary in which the key is the domain name and the value:
            a list of words
                in a list of sentences
                    in a list of documents
       """

    data = {}  # a dictionary with
    to_read = len(os.listdir(directory))
    idx = 0
    read_files=[]
    # read the train_data from all domains


    # shuffle the files in each domain
    files = os.listdir(directory)
    random.shuffle(files)
    for f in files:
        data_document = []
        # if the limit for the domain has been reached pass to the next one

        if f.endswith(".txt"):
            read_files.append(f)
            # read the train_data form a specific document
            with open(os.path.join(directory, f)) as file:

                data_sentences = []  # a sentence for each line
                # for every line in the file
                for line in file.readlines():

                    # split the line into sentences
                    sentences = sent_tokenize(line)

                    for sentence in sentences:

                        # split the line by words
                        split = sentence.lower().strip().split()

                        if filter_:
                            split = filter_document(split)

                            data_sentences.append(split)

                    # clean train_data sentence
                    data_sentences = [elem for elem in data_sentences if elem]

                data_document += data_sentences
                # clean train_data document
                data_document = [elem for elem in data_document if elem]

        file_id=f.split("_")[1].split(".")[0]
        data[file_id]=data_document

        idx += 1
        print(colored(f"\rReading file {idx}/{to_read}", color="cyan"), end="")

    print("")
    return data


def read_test(model, data_dir):


    # get the embed dict
    metadata = f"../Good_models/Model_{model}/Model/metadata.tsv"
    embed = f"../Good_models/Model_{model}/Operators/embed.pickle"
    embed_dict = generate_dict(metadata, embed)


    # read the documents
    data = read_test_document(data_dir, True)
    data = {w: sentences2embed(embed_dict, v)  for w, v in data.items()}



    # vectoralize train_data
    data = vectorize(data)

    return data

def read_dataset(model, data_dir, domain_words=200):
    """
    Read and create the train_data for training testing
    :param model: the number of the Good_model/Model to use for the embeddings and the metadata
    :param data_dir: the path to the train_data (train or dev)
    :returns:
        a list of tuples where the first element is the class and the second a vecotr
   """

    # get the embed dict
    metadata = f"../Good_models/Model_{model}/Model/metadata.tsv"
    embed = f"../Good_models/Model_{model}/Operators/embed.pickle"
    embed_dict = generate_dict(metadata, embed)

    # read the documents
    data,files = read_documents(data_dir, domain_words, True)
    data = {w: [sentences2embed(embed_dict, elem) for elem in v] for w, v in data.items()}


    # vectoralize train_data
    data = vectorize(data)

    return data,files,embed_dict


def vectorize(data):
    """
    Transform every elem of the train_data into a vector
    :param data: dictionary, where the key is a domain and the value is a list of documents
                composed by a list of sentences
                composed by a list of words
                which is a vector
    :return: a dictionary in which the key is the domain and the value a vector
    """

    new_dict = {}

    total = len(data)
    idx = 1

    # create a pool
    pool = Pool(5)

    for domain, documents in data.items():
        new_dict[domain] = pool.map(parallel_vectorize, documents)
        print(f"\rVecotrized {idx}/{total}", end="")
        idx += 1

    return new_dict



def parallel_vectorize(sentences):
    mean_s = []

    for sentence in sentences:
        mean_s.append(np.mean(sentence, axis=0))

    return np.mean(mean_s, axis=0)


def split_XYdata(data, valid_set=0.0):
    """
    Split the train_data into x,y vectors
    :param data: dictionary where key is the y and value is a list of samples
    :param valid_set: the dimension of the valid set in perc
    :returns:
        train: a tuple of two 1D array (sample, label)
        y_label_dict: a dictionary which maps a train_data key to an int
        y_train:  a tuple of two 1D array (sample, label)

    """

    print("Splitting X/Y Train")
    y_label_dict = {}

    for k in data.keys():
        y_label_dict[k] = len(y_label_dict)

    train_set = []
    for k, v in data.items():
        for d in v:
            train_set.append((y_label_dict[k], d))


    # print("Saving train_data")
    # with open("train_data.json","wb") as file:
    #     ujson.dump(train_set,file)

    random.shuffle(train_set)

    y_train = [y for y, _ in train_set]
    x_train = [x for _, x in train_set]
    x_train = np.array(x_train)

    valid_size = round(len(x_train) * valid_set)
    x_valid = x_train[:valid_size]
    y_valid = y_train[:valid_size]
    x_train = x_train[valid_size + 1:]
    y_train = y_train[valid_size + 1:]
    x_train = np.array(x_train)

    train = (x_train, y_train)
    valid = (x_valid, y_valid)

    return train, y_label_dict, valid


def sentences2embed(dictionary, sentences):
    """
    Embed a list of sentences
    :param dictionary: the dictionary used for word to embed conversion
    :param sentences: a list of sentences which is a list of list of words
    :return: a vector of embedded words in a list of words in a list of sentnces
    """

    found = 0
    total = 0

    embed_sentences = []

    for sentence in sentences:

        embed_words = []

        for word in sentence:
            try:
                word = dictionary[word]
                found += 1
            except KeyError:
                word = dictionary['UNK']

            embed_words.append(word)
            total += 1

        embed_sentences.append(embed_words)

    # print(f"\rWords found {found/total*100}%",end="")

    return embed_sentences

def read_files(files,embed_dict,filter_,min_sentence_len=0):

    data=[]

    to_read=len(files)
    idx=0

    for f in files:
        with open(f) as file:
            data_sentences = []  # a sentence for each line
            # for every line in the file
            for line in file.readlines():

                # split the line into sentences
                sentences = sent_tokenize(line)

                for sentence in sentences:


                    # split the line by words
                    split = sentence.lower().strip().split()


                    if filter_:
                        split = filter_document(split)

                    if len(split)<min_sentence_len:continue

                    data_sentences.append(split)


                # clean train_data sentence
            data_sentences = [elem for elem in data_sentences if elem]
            data_sentences=sentences2embed(embed_dict,data_sentences)
            data_sentences=parallel_vectorize(data_sentences)
            data.append(data_sentences)
            # clean train_data document
        idx+=1
        #print(f"\rReading file {idx}/{to_read}",end="")

    return data
