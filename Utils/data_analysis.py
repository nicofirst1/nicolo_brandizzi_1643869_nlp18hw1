import os
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score,accuracy_score
import numpy as np
from HomeWork1.Word2Vec.data_processing import filter_document


def count_cat_words(path2dir):
    """
    Count the number of words for each category
    :param path2dir: the path for the category dir
    :returns:
        word_num: the number of words
        idx: the number of documents
        cahr_num : the number of characters
    """

    word_list = []

    idx = 0
    for f in os.listdir(path2dir):
        if f.endswith(".txt"):
            idx += 1
            with open(os.path.join(path2dir, f), "r+") as file:
                word_list += filter_document(file.readlines())

    word_num = len(word_list)
    char_num = len("".join(word_list))

    return word_num, idx, char_num


def count_all_words(path2dir):
    """
    Count the number of words for all the  train_data
    :param path2dir: the path for the main dir
    :return: a list of words
    """
    words = 0

    for domain in os.listdir(path2dir):
        words_num, doc_num,char_num = count_cat_words(os.path.join(path2dir, domain))
        words += words_num

    return words


def mean_wordXcat(path2dir):
    """
    Get useful statistic for a dataset
    :param path2dir: the path to the dataset dir
    :return:
    a dict where a key is the category and the values are the following:
        total_words : the number of words in that category
        total_doc : the number of documents for that category
        wordsXdoc : the mean number of words per document in the category
        total_char: the number of character per category
    Plus a 'general' key which has as value the same dict as before but computed on the whole dataset
    """

    words = 0  # the number of total words
    documents = 0  # the number of total documents
    characters = 0

    res = {}  # the res dictionary

    domains = len(os.listdir(path2dir))
    idx = 0
    # for every category in the dataset
    for domain in os.listdir(path2dir):
        # count the number of words and documents
        words_num, doc_num, char_num = count_cat_words(os.path.join(path2dir, domain))
        # add them to the total sum
        words += words_num
        documents += doc_num
        characters += char_num

        # save the local parameters
        res[domain] = {'total_words': words_num, 'total_doc': doc_num, 'wordsXdoc': words_num / doc_num,
                       'total_char': char_num}
        # print the number of remaning categories
        idx += 1
        print(f"Categories : {idx}/{domains}")

    # save the general parameters
    res['GENERAL'] = {'total_words': words, 'total_doc': documents, 'wordsXdoc': words / documents,
                      'total_char': characters}

    return res


def create_table(path2dir):
    """
    Create a MarkDown table from a dictionary
    :param path2dir: the path to the dataset dir
    :return: a string representing the table
    """

    # get he dictionary
    words = mean_wordXcat(path2dir)

    # initialize the table
    table = """
|CATEGORY       | tot words     | tot doc    | wordsXDoc | tot char  |
| ------------- |:-------------:|:----------:|:---------:|----------:|
"""

    # add the train_data
    for key, val in words.items():
        table += f"|{key}|{val['total_words']}|{val['total_doc']}|{val['wordsXdoc']}|{val['total_char']}|\n"

    return table


def plot_with_labels(save_dir,final_embeddings, reverse_dictionary,name):


    text = input(f"Do you want to plot the TNSE for {name}? [y/n]")

    if "y" not in text.lower():
        return


    print("Plotting embedding with TNSE...")

    filename = save_dir+f"{name}_tnse.png"

    tsne = TSNE(
        perplexity=30, n_components=2, init='pca', n_iter=5000, method='exact')
    plot_only = 500
    low_dim_embs = tsne.fit_transform(final_embeddings[:plot_only, :])
    labels = [reverse_dictionary[i] for i in range(plot_only)]

    assert low_dim_embs.shape[0] >= len(labels), 'More labels than embeddings'
    plt.figure(figsize=(18, 18))  # in inches
    for i, label in enumerate(labels):
        x, y = low_dim_embs[i, :]
        plt.scatter(x, y)
        plt.annotate(
            label,
            xy=(x, y),
            xytext=(5, 2),
            textcoords='offset points',
            ha='right',
            va='bottom')

    plt.savefig(filename)

    print("...plotting done")


def pred_infos(y_pred, y_true, recall=True, precision=True, f1=True, confusion=True, accuracy=True):
    """
    Print usefull infos about a prediction
    :param y_pred: 1D array, the predicted y
    :param y_true: 1D array, the true y
    :param recall: bool, estimate recall score
    :param precision: bool, estimate precision score
    :param f1: bool, estimate f1 score
    :param confusion: bool, plot confusion matrix
    :return: None
    """

    print("Estimating prediction infos")

    if precision:
        p = precision_score(y_true, y_pred, average="weighted")
        print(f"Precision {round(p*100,2)}")

    if recall:
        r = recall_score(y_true, y_pred, average="weighted")
        print(f"Recall {round(r*100,2)}")

    if f1:
        f1_s = f1_score(y_true, y_pred, average="weighted")
        print(f"F1 {round(f1_s*100,2)}")

    if accuracy:
        accuracy=accuracy_score(y_true,y_pred)
        print(f"accuracy {round(accuracy*100,2)}")


    if confusion:
        confusion = confusion_matrix(y_true, y_pred)
        confusion = confusion / np.linalg.norm(confusion)
        np.fill_diagonal(confusion, 0)
        plt.matshow(confusion, cmap=plt.cm.gray)
        plt.show()
