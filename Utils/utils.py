import math
import os
import shutil
from asyncio import sleep
import pickle
import random
from multiprocessing.pool import Pool

import numpy as np
from nltk import sent_tokenize
from sklearn.decomposition import PCA
from termcolor import colored
from functools import reduce


def clean_logs(dirs):
    text = input("Do you want to clear the Model dir?[y/n]")

    if "y" not in text:
        print("Not cleaning the dir")
        return

    print("Cleaning the direcotry")
    for dir in os.listdir(dirs):
        for f in os.listdir(os.path.join(dirs, dir)):
            os.remove(os.path.join(dirs + dir, f))


def beep():
    duration = 0.2  # second
    freq = 540  # Hz

    for j in range(3):
        for i in range(3):
            os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))
            sleep(0.2)
            os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % (duration, freq))
        sleep(1)


def save_good_model(dir):
    text = input("Do you want to save the model? [y/n]")

    if "y" not in text.lower():
        return

    save_dir = "Good_models/"
    idx = 1

    for _, dirnames, _ in os.walk(save_dir):
        # ^ this idiom means "we won't be using this value"
        idx += len(dirnames)

    save_dir += f"Model_{idx}/"
    os.makedirs(save_dir)
    print("Saving...")

    shutil.copytree(dir, save_dir + dir)

    print(f"Model saved in {save_dir}")
    return


def read_dictionary(path2dir, words):
    # load the dict with picle
    with open(path2dir, "rb") as file:
        dictionary = pickle.load(file)
        # generate reverse_dict and train_data
        reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
        data = []
        for word in words:
            index = dictionary.get(word, 0)
            data.append(index)

        print(colored("...train_data loaded from pickle dictionry", color="blue"))
        return data, dictionary, reverse_dictionary




def PPA(embedding_matrix, top_components=7):
    """
    Computes the post-processing algorithm on the embedding vector
    :param embedding_matrix: the mebedding matrix
    :param top_components: the top n components you want to pop
    :return: the emedded matrix
    """

    print("Executing PPA...")

    embedding_matrix = embedding_matrix - np.mean(embedding_matrix)

    pca = PCA()

    pca_cmp = pca.fit_transform(embedding_matrix)

    new_embed_matrix = []

    for v in embedding_matrix:
        new_v = v
        for i in range(top_components):
            u = pca_cmp[i, :]
            new_v -= np.dot(np.dot(np.transpose(u), v), u)
        new_embed_matrix.append(new_v)

    print("...done")

    return new_embed_matrix


def embedding_dim_reduction(embedding_matrix, top_component=7):
    """
    Reduces the dimensionality of the embeddings
    :param embedding_matrix: the embedding matrix
    :param top_component: the number of top components to delete
    :return: the reducted embedding matrix
    """

    embedding_matrix = PPA(embedding_matrix, top_component)
    print("Executing PCA")
    pca = PCA()
    embedding_matrix = pca.fit_transform(embedding_matrix)
    print("...done")

    embedding_matrix = PPA(embedding_matrix, top_component)
    return embedding_matrix


def tfidf(word, document, documents):
    return tf(word,document)*idf(word,documents)



def tf(word, document):
    c=len([w for w in document if w==word])
    return c/len(document)

def idf(word, documents):
    words=flatten(documents)
    c=sum(1 for w in words if w==word)+1

    return math.log(len(documents)/c)


def flatten(l):
    flat_list=[]
    for sublist in l:
        for item in sublist:
            flat_list.append(item)

    return flat_list

def recover_key(dicty,value):
    for a_key in dicty.keys():
        if (dicty[a_key] == value):
            return a_key